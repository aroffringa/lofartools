#include <iostream>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>

int main(int argc, char** argv) {
  if (argc < 2)
    std::cout << "Syntax: checkms <ms>\n";
  else {
    try {
      casacore::MeasurementSet ms(argv[1]);
      size_t nrow = ms.nrow();
      casacore::ArrayColumn<casacore::Complex> column(
          ms,
          casacore::MeasurementSet::columnName(casacore::MeasurementSet::DATA));
      if (nrow == 0)
        std::cout << "FAILURE (no data in set)\n";
      else {
        // Read 100 rows at random
        column.get(0);
        for (size_t i = 0; i != 100; ++i) {
          size_t randomRow = rand() % ms.nrow();
          column.get(randomRow);
        }
        column.get(ms.nrow() - 1);
        std::cout << "OK     " << ms.nrow() << " rows.\n";
      }
    } catch (std::exception& e) {
      std::cout << "FAILURE: " << e.what() << '\n';
    } catch (...) {
      std::cout << "FAILURE (unknown exception)\n";
    }
  }
}
