#include <boost/test/unit_test.hpp>

#include "../math/gaussianconversions.h"

#include <aocommon/image.h>

BOOST_AUTO_TEST_SUITE(gaussian_fitter)

BOOST_AUTO_TEST_CASE(conversions) {
  const long double sigmaToBeam = 2.0L * sqrtl(2.0L * logl(2.0L));
  double gMax, gMin, gAngle;
  double outMax, outMin, outAngle;
  double sxsx, sxsy, sysy;

  gMax = sigmaToBeam;  // sx = 1
  gMin = sigmaToBeam;
  gAngle = 0.0;
  math::ToGaussianCovariance(gMax, gMin, gAngle, sxsx, sxsy, sysy);

  BOOST_CHECK_CLOSE_FRACTION(sxsx, 1.0, 1e-4);  // a = 1 / (2 sx ^2) = 0.5
  BOOST_CHECK_CLOSE_FRACTION(sxsy, 0.0, 1e-4);
  BOOST_CHECK_CLOSE_FRACTION(sysy, 1.0, 1e-4);

  math::FromGaussianCovariance(sxsx, sxsy, sysy, outMax, outMin, outAngle);
  BOOST_CHECK_CLOSE_FRACTION(outMax, sigmaToBeam, 1e-4);
  BOOST_CHECK_CLOSE_FRACTION(outMin, sigmaToBeam, 1e-4);

  gMax = sigmaToBeam;        // sx = 1
  gMin = sigmaToBeam * 0.5;  // sy = 0.5
  gAngle = 0.0;
  math::ToGaussianCovariance(gMax, gMin, gAngle, sxsx, sxsy, sysy);

  BOOST_CHECK_CLOSE_FRACTION(sxsx, 1.0, 1e-4);
  BOOST_CHECK_CLOSE_FRACTION(sxsy, 0.0, 1e-4);
  BOOST_CHECK_CLOSE_FRACTION(sysy, 0.25, 1e-4);

  math::ToGaussianCovariance(1.0, 0.5, gAngle, sxsx, sxsy, sysy);
  math::FromGaussianCovariance(sxsx, sxsy, sysy, outMax, outMin, outAngle);
  BOOST_CHECK_CLOSE_FRACTION(outMax, 1.0, 1e-4);
  BOOST_CHECK_CLOSE_FRACTION(outMin, 0.5, 1e-4);
  outAngle += 2.0 * M_PI;
  while (outAngle > 0.5 * M_PI) outAngle -= M_PI;
  BOOST_CHECK_CLOSE_FRACTION(outAngle, 0.0, 1e-4);

  for (double x = 0.0; x < 2.0; x += 0.1) {
    gAngle = x * M_PI;
    math::ToGaussianCovariance(1.0, 0.5, gAngle, sxsx, sxsy, sysy);
    math::FromGaussianCovariance(sxsx, sxsy, sysy, outMax, outMin, outAngle);
    BOOST_CHECK_CLOSE_FRACTION(outMax, 1.0, 1e-4);
    BOOST_CHECK_CLOSE_FRACTION(outMin, 0.5, 1e-4);
    outAngle += 2.0 * M_PI;
    while (outAngle > gAngle + 0.5 * M_PI) outAngle -= M_PI;
    BOOST_CHECK_CLOSE_FRACTION(outAngle * 180.0 / M_PI, gAngle * 180.0 / M_PI,
                               1e-4);
  }
}

BOOST_AUTO_TEST_SUITE_END()
