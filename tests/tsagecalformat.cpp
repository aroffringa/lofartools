#include <boost/test/unit_test.hpp>

#include "../model/sagecalformat.h"

BOOST_AUTO_TEST_SUITE(sagecal_format)

const std::string kRefSources =
    R"(## name h m s d m s I Q U V si0 si1 si2 RM eX(rad) exY(rad) PA(rad) freq0
P_a_C0 0 0 0 180 0 0 3.14 0 0 0 0 0 0 0 0 0 -1.5708 1.5e+08
G_b_C0 16 21 38.03 -57 17 44.81 1 0 0 0 -0.5 0.0434294 -0.00188612 0 1.5 2.5 1.9292 1.75e+08
)";

const std::string kRefClusters =
    R"(## cID chunk_size source1 source2 ...
0 1 P_a_C0 G_b_C0
)";

BOOST_AUTO_TEST_CASE(write) {
  Model model;

  ModelSource a;
  a.SetName("a");
  a.SetClusterName("my_cluster");
  ModelComponent comp_a;
  comp_a.SetPosRA(0.0);
  comp_a.SetPosDec(M_PI);
  PowerLawSED pw_a(150e6, 3.14);
  comp_a.SetSED(pw_a);
  a.AddComponent(comp_a);
  model.AddSource(a);

  ModelSource b;
  b.SetName("b");
  b.SetClusterName("my_cluster");
  ModelComponent comp_b;
  comp_b.SetType(ModelComponent::GaussianSource);
  comp_b.SetPosRA(-2.0);
  comp_b.SetPosDec(-1.0);
  PowerLawSED pw_b;
  pw_b.SetFromStokesIFit(175e6, {1.0, -0.5, 0.1, -0.01});
  comp_b.SetSED(pw_b);
  comp_b.SetMajorAxis(1.5);
  comp_b.SetMinorAxis(2.5);
  comp_b.SetPositionAngle(3.5);
  b.AddComponent(comp_b);
  model.AddSource(b);

  std::ostringstream sources;
  std::ostringstream custers;
  model::sagecal::Write(model, sources, custers, false, 1);
  BOOST_CHECK_EQUAL(sources.str(), kRefSources);
  BOOST_CHECK_EQUAL(custers.str(), kRefClusters);
}

BOOST_AUTO_TEST_CASE(read) {
  std::istringstream sources(kRefSources);
  std::istringstream custers(kRefClusters);
  const Model model = model::sagecal::Read(sources, custers);
  BOOST_REQUIRE_EQUAL(model.SourceCount(), 2);

  const ModelSource& a = model.Source(0);
  BOOST_CHECK_EQUAL(a.Name(), "P_a_C0");
  BOOST_CHECK_EQUAL(a.ClusterName(), "0");
  BOOST_REQUIRE_EQUAL(a.ComponentCount(), 1);
  const ModelComponent& comp_a = a.Component(0);
  BOOST_CHECK(comp_a.Type() == ModelComponent::PointSource);
  BOOST_CHECK_CLOSE_FRACTION(comp_a.PosRA(), 0.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(comp_a.PosDec(), M_PI, 1e-6);
  BOOST_REQUIRE(comp_a.HasPowerLawSED());
  const PowerLawSED& sed_a = dynamic_cast<const PowerLawSED&>(comp_a.SED());
  BOOST_REQUIRE_EQUAL(sed_a.NTerms(), 4);
  double ref_frequency_a;
  double iquv_a[4];
  std::vector<double> si_terms_a;
  sed_a.GetData(ref_frequency_a, iquv_a, si_terms_a);
  BOOST_CHECK_CLOSE_FRACTION(ref_frequency_a, 150e6, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_a[0], 3.14, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_a[1], 0.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_a[2], 0.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_a[3], 0.0, 1e-6);
  BOOST_REQUIRE_EQUAL(si_terms_a.size(), 3);
  BOOST_CHECK_CLOSE_FRACTION(si_terms_a[0], 0.0, 1e-5);
  BOOST_CHECK_CLOSE_FRACTION(si_terms_a[1], 0.0, 1e-5);
  BOOST_CHECK_CLOSE_FRACTION(si_terms_a[2], 0.0, 1e-5);

  const ModelSource& b = model.Source(1);
  BOOST_CHECK_EQUAL(b.Name(), "G_b_C0");
  BOOST_CHECK_EQUAL(b.ClusterName(), "0");
  BOOST_REQUIRE_EQUAL(b.ComponentCount(), 1);
  const ModelComponent& comp_b = b.Component(0);
  BOOST_CHECK(comp_b.Type() == ModelComponent::GaussianSource);
  BOOST_CHECK_CLOSE_FRACTION(comp_b.PosRA(), 2.0 * M_PI - 2.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(comp_b.PosDec(), -1.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(comp_b.MajorAxis(), 1.5, 1e-5);
  BOOST_CHECK_CLOSE_FRACTION(comp_b.MinorAxis(), 2.5, 1e-5);
  BOOST_CHECK_CLOSE_FRACTION(comp_b.PositionAngle(), 3.5, 1e-5);
  BOOST_REQUIRE(comp_b.HasPowerLawSED());
  const PowerLawSED& sed_b = dynamic_cast<const PowerLawSED&>(comp_b.SED());
  BOOST_REQUIRE_EQUAL(sed_b.NTerms(), 4);
  double ref_frequency_b;
  double iquv_b[4];
  std::vector<double> si_terms_b;
  sed_b.GetData(ref_frequency_b, iquv_b, si_terms_b);
  BOOST_CHECK_CLOSE_FRACTION(ref_frequency_b, 175e6, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_b[0], 1.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_b[1], 0.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_b[2], 0.0, 1e-6);
  BOOST_CHECK_CLOSE_FRACTION(iquv_b[3], 0.0, 1e-6);
  BOOST_REQUIRE_EQUAL(si_terms_b.size(), 3);
  BOOST_CHECK_CLOSE_FRACTION(si_terms_b[0], -0.5, 1e-5);
  BOOST_CHECK_CLOSE_FRACTION(si_terms_b[1], 0.1, 1e-5);
  BOOST_CHECK_CLOSE_FRACTION(si_terms_b[2], -0.01, 1e-5);
}

BOOST_AUTO_TEST_SUITE_END()
