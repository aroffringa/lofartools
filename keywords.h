#ifndef KEYWORDS_H
#define KEYWORDS_H

#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>

struct Keywords {
  double weight, nVis, effNVis, normalizationFactor;

  Keywords() : weight(0.0), nVis(0.0), effNVis(0.0), normalizationFactor(0.0) {}

  Keywords(aocommon::FitsReader& reader)
      : weight(1.0), nVis(0.0), effNVis(0.0), normalizationFactor(1.0) {
    if (!reader.ReadDoubleKeyIfExists("WSCIMGWG", weight))
      std::cout << "Warning: file did not have WSCIMGWG field.\n";
    if (!reader.ReadDoubleKeyIfExists("WSCNVIS", nVis))
      std::cout << "Warning: file did not have WSCNVIS field.\n";
    if (!reader.ReadDoubleKeyIfExists("WSCENVIS", effNVis))
      std::cout << "Warning: file did not have WSCENVIS field.\n";
    if (!reader.ReadDoubleKeyIfExists("WSCNORMF", normalizationFactor))
      std::cout << "Warning: file did not have WSCNORMF field. Assuming NO "
                   "NORMALIZATION was done!\n";
  }

  void Set(aocommon::FitsWriter& writer) {
    writer.SetExtraKeyword("WSCIMGWG", weight);
    writer.SetExtraKeyword("WSCNVIS", nVis);
    writer.SetExtraKeyword("WSCENVIS", effNVis);
    writer.SetExtraKeyword("WSCNORMF", normalizationFactor);
  }

  Keywords& operator+=(const Keywords& keywords) {
    weight += keywords.weight;
    nVis += keywords.nVis;
    effNVis += keywords.effNVis;
    normalizationFactor += keywords.normalizationFactor;
    return *this;
  }
};

#endif
