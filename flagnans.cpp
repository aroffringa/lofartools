#include <iostream>
#include <stdexcept>
#include <set>
#include <map>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/tables/Tables/ScalarColumn.h>

using namespace casacore;

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout
        << "Usage: flagnans [-datacolumn <COL>] [-nowrite] <ms>\n"
           "Flags all correlations of baselines that should not be used.\n";
  } else {
    std::string dataColumnName = "CORRECTED_DATA";

    bool noWrite = false;
    size_t argi = 1;
    while (argi < size_t(argc) && argv[argi][0] == '-') {
      std::string p = &argv[argi][1];
      if (p == "datacolumn") {
        ++argi;
        dataColumnName = argv[argi];
      } else if (p == "nowrite") {
        noWrite = true;
      } else
        throw std::runtime_error("Bad option");

      ++argi;
    }

    MeasurementSet ms(argv[argi], Table::Update);

    /**
     * Read some meta data from the measurement set
     */
    MSSpectralWindow spwTable = ms.spectralWindow();
    size_t spwCount = spwTable.nrow();
    if (spwCount != 1)
      throw std::runtime_error("Set should have exactly one spectral window");

    ROScalarColumn<int> numChanCol(
        spwTable,
        MSSpectralWindow::columnName(MSSpectralWindowEnums::NUM_CHAN));
    size_t channelCount = numChanCol.get(0);
    if (channelCount == 0) throw std::runtime_error("No channels in set");

    ROScalarColumn<int> ant1Column(ms, ms.columnName(MSMainEnums::ANTENNA1));
    ROScalarColumn<int> ant2Column(ms, ms.columnName(MSMainEnums::ANTENNA2));
    ArrayColumn<bool> flagColumn(ms, ms.columnName(MSMainEnums::FLAG));
    ArrayColumn<casacore::Complex> dataColumn(ms, dataColumnName);

    if (ms.nrow() == 0) throw std::runtime_error("Table has no rows (no data)");
    IPosition dataShape = dataColumn.shape(0);
    size_t polarizationCount = dataShape[0];

    if (noWrite)
      std::cout << "Counting... " << std::flush;
    else
      std::cout << "Flagging... " << std::flush;

    /**
     * Flag
     */
    Array<bool> flagArray(dataShape);
    Array<Complex> dataArray(dataShape);

    size_t crossCount = 0, autoCount = 0, infCount = 0;
    for (size_t rowIndex = 0; rowIndex != ms.nrow(); ++rowIndex) {
      size_t a1 = ant1Column.get(rowIndex), a2 = ant2Column.get(rowIndex);
      dataColumn.get(rowIndex, dataArray);
      flagColumn.get(rowIndex, flagArray);

      bool rowIsChanged = false;
      Array<bool>::contiter fi = flagArray.cbegin();
      Array<Complex>::contiter di = dataArray.cbegin();
      for (size_t ch = 0; ch != channelCount; ++ch) {
        bool isChannelFlagged = false, wasFlagged = true;
        for (size_t p = 0; p != polarizationCount; ++p) {
          bool flag = *(fi + p);
          Complex c = *(di + p);
          bool isFinite = std::isfinite(c.real()) && std::isfinite(c.imag());
          if (!isFinite) ++infCount;
          if (!flag) wasFlagged = false;
          isChannelFlagged = isChannelFlagged || flag || !isFinite;
        }
        if (isChannelFlagged) {
          if (!wasFlagged) {
            if (a1 == a2)
              ++autoCount;
            else
              ++crossCount;
          }
          for (size_t p = 0; p != polarizationCount; ++p) {
            *(fi + p) = true;
            *(di + p) = 0.0;
          }
          rowIsChanged = true;
        }
        fi += polarizationCount;
        di += polarizationCount;
      }
      if (rowIsChanged && !noWrite) {
        dataColumn.put(rowIndex, dataArray);
        flagColumn.put(rowIndex, flagArray);
      }
    }

    if (noWrite)
      std::cout << "DONE (" << crossCount << " cross- and " << autoCount
                << " auto-correlated samples should be flagged, infcount="
                << infCount << ")\n";
    else
      std::cout << "DONE (flagged " << crossCount << " cross- and " << autoCount
                << " auto-correlated samples, infcount=" << infCount << ")\n";
  }

  return 0;
}
