#include "model/model.h"

#include "gnuplot.h"
#include "gnustatplot.h"

#include <aocommon/units/angle.h>

#include <iostream>
#include <vector>

std::string desc(const ModelSource& source) {
  std::ostringstream s;
  s << source.Name() << " (J" << RaDecCoord::RAToString(source.MeanRA()) << " "
    << RaDecCoord::DecToString(source.MeanDec()) << ", "
    << source.TotalFlux(150e6, aocommon::Polarization::StokesI) << " Jy)";
  return s.str();
}

void findMatches(const Model& model, const ModelSource& position,
                 std::vector<const ModelSource*>& matches, double separation) {
  long double matchRA = position.MeanRA(), matchDec = position.MeanDec();
  for (Model::const_iterator s = model.begin(); s != model.end(); ++s) {
    long double ra = s->MeanRA(), dec = s->MeanDec();

    double dist = aocommon::ImageCoordinates::AngularDistance<long double>(
        ra, dec, matchRA, matchDec);
    // std::cout << "dist=" << Angle::ToNiceString(dist) << "\n";

    if (dist <= separation) matches.push_back(&*s);
  }
}

bool isSourceSmaller(const ModelSource* lhs, const ModelSource* rhs) {
  return (*lhs) < (*rhs);
}

int main(int argc, char* argv[]) {
  if (argc <= 1) {
    std::cout << "Syntax: plottime <model1> [<model2> ..]\n";
    return -1;
  }
  const double separation = 3.0 * (M_PI / 180.0) / 60.0;  // 3 arcmin
  const double refFreq = 150e6;
  const double discardMatchLevel =
      40.0;  // if source varies more than this percentage, skip
  const double brightestLimit = 3.0;  // Jansky

  std::vector<Model> models;
  for (int argi = 1; argi != argc; ++argi) {
    std::string f(argv[argi]);
    std::cout << "Opening " << f << "...\n";
    models.push_back(Model(f));
  }

  GNUPlot offsetPlot("offset", "Time (h)",
                     "Total offset from init. pos. (arcsec)"),
      offsetBrightest("offset-brightest", "Time (h)",
                      "Total offset from init. pos. (arcsec)"),
      raOffsetPlot("ra-offset", "Time (h)",
                   "RA offset from init. pos. (arcsec)"),
      decOffsetPlot("dec-offset", "Time (h)",
                    "Dec offset from init. pos. (arcsec)");
  GNUStatPlot offsetStatPlot, decStatPlot, raStatPlot;

  std::cout << "Matching (separation="
            << aocommon::units::Angle::ToNiceString(separation) << ")...\n";
  const Model& firstModel = models.front();
  size_t firstSourceIndex = 0;
  for (Model::const_iterator s = firstModel.begin(); s != firstModel.end();
       ++s) {
    std::cout << "- Source " << desc(*s) << ':';
    std::vector<const ModelSource*> matches;
    findMatches(firstModel, *s, matches, separation);
    double firstFlux = s->TotalFlux(refFreq, aocommon::Polarization::StokesI);
    bool processSource =
        (s->TotalFlux(refFreq, aocommon::Polarization::StokesI) > 1.0);
    if (processSource && matches.size() > 1) {
      std::sort(matches.begin(), matches.end(), &isSourceSmaller);
      const ModelSource* strongestSource = matches.front();
      if (&*s != strongestSource) {
        std::cout << "Skipping source " << desc(*s)
                  << " because it has a stronger component closeby.\n";
        processSource = false;
      }
    }
    std::cout << " sources nearby: " << (matches.size() - 1);
    if (!processSource)
      std::cout << ", skipping.\n";
    else {
      std::vector<const ModelSource*> measurements(1, &*s);

      for (size_t mIndex = 1; mIndex != models.size(); ++mIndex) {
        const Model& secondModel = models[mIndex];
        matches.clear();
        findMatches(secondModel, *s, matches, separation);
        if (matches.empty()) {
          // std::cout << "  No match found for source " << desc(*s) << " in
          // model " << (mIndex+1) << "\n";
          measurements.push_back(nullptr);
        } else {
          std::sort(matches.begin(), matches.end(), &isSourceSmaller);
          if (matches.size() > 1) {
            // std::cout << "  Warning: source " << desc(*s) << " has multiple
            // matches in model " << (mIndex+1) << ", skipping measurement.\n";
            measurements.push_back(nullptr);
          } else {
            const ModelSource* strongestSource = matches.front();
            double matchingFlux = strongestSource->TotalFlux(
                refFreq, aocommon::Polarization::StokesI);
            double mismatch = 2.0 * fabs(matchingFlux - firstFlux) /
                              (matchingFlux + firstFlux);
            if (mismatch > discardMatchLevel * 0.01) {
              std::cout << "\n  Flux mismatch=" << mismatch * 100.0 << "%, >"
                        << discardMatchLevel << "%, skipping measurement.\n";
              measurements.push_back(nullptr);
            } else {
              measurements.push_back(strongestSource);
            }
          }
        }
      }

      // measurements has now been filled.
      std::string caption;
      double lineWidth = 1.0;
      GNUPlot::Line* offsetBrighestLine = nullptr;
      if (s->TotalFlux(refFreq, aocommon::Polarization::StokesI) >
          brightestLimit) {
        caption = desc(*s);
        lineWidth = 3.0;
        offsetBrighestLine =
            offsetBrightest.AddLine(s->Name() + ".txt", caption, lineWidth);
      }
      GNUPlot::Line *offsetLine = offsetPlot.AddLine(s->Name() + ".txt",
                                                     caption, lineWidth),
                    *raOffsetLine = raOffsetPlot.AddLine(s->Name() + "_ra.txt",
                                                         caption, lineWidth),
                    *decOffsetLine = decOffsetPlot.AddLine(
                        s->Name() + "_dec.txt", caption, lineWidth);
      aocommon::UVector<double> offsetValues(
          measurements.size(), std::numeric_limits<double>::quiet_NaN()),
          raValues(measurements.size(),
                   std::numeric_limits<double>::quiet_NaN()),
          decValues(measurements.size(),
                    std::numeric_limits<double>::quiet_NaN());
      if (firstSourceIndex == 0) {
        // first source? initialize x-axis
        aocommon::UVector<double> xaxis(measurements.size());
        for (size_t i = 0; i != measurements.size(); ++i)
          xaxis[i] = i * 2.0 / 60.0;
        offsetStatPlot.SetXValues(xaxis);
        raStatPlot.SetXValues(xaxis);
        decStatPlot.SetXValues(xaxis);
      }
      size_t nMeas = 0;
      double sDisp =
          5.0 * firstSourceIndex;  // arcmin, displacement between lines
      for (size_t i = 0; i != measurements.size(); ++i) {
        const ModelSource* source = measurements[i];
        if (source != nullptr) {
          ++nMeas;
          long double time = i * 2.0 / 60.0, ra1 = s->MeanRA(),
                      dec1 = s->MeanDec(), ra2 = source->MeanRA(),
                      dec2 = source->MeanDec(),
                      dist = aocommon::ImageCoordinates::AngularDistance(
                          ra1, dec1, ra2, dec2);
          offsetValues[i] = dist * 180.0 * 60.0 * 60.0 / M_PI;
          raValues[i] = (ra2 - ra1) * 180.0 * 60.0 * 60.0 / M_PI;
          decValues[i] = (dec2 - dec1) * 180.0 * 60.0 * 60.0 / M_PI;
          offsetLine->AddPoint(time, offsetValues[i] + sDisp);
          if (offsetBrighestLine != nullptr)
            offsetBrighestLine->AddPoint(time, offsetValues[i] + sDisp);
          raOffsetLine->AddPoint(time, raValues[i] + sDisp);
          decOffsetLine->AddPoint(time, decValues[i] + sDisp);
        }
      }
      offsetStatPlot.AddYSet(offsetValues);
      raStatPlot.AddYSet(raValues);
      decStatPlot.AddYSet(decValues);
      std::cout << ", " << nMeas << " measurements. ";
      ++firstSourceIndex;
    }
  }

  GNUPlot offsetStatOutput("offset-stat", "Time (h)",
                           "Total offset from init. pos. (arcsec)"),
      raStatOutput("ra-stat", "Time (h)", "RA offset from init. pos. (arcsec)"),
      decStatOutput("dec-stat", "Time (h)",
                    "Dec offset from init. pos. (arcsec)");
  offsetStatOutput.SetTitle("Average offset over all sources > 1 Jy");
  raStatOutput.SetTitle(
      "Average right ascension offset over all sources > 1 Jy");
  decStatOutput.SetTitle("Average declination offset over all sources > 1 Jy");
  offsetStatPlot.Plot(offsetStatOutput);
  raStatPlot.Plot(raStatOutput);
  decStatPlot.Plot(decStatOutput);
}
