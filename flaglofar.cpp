#include <iostream>
#include <stdexcept>
#include <set>
#include <map>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/tables/Tables/ScalarColumn.h>

using namespace casacore;

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout
        << "Usage: flaglofar [-keep-intra] [-keep-cs013] [-station name] <ms>\n"
           "Flags all correlations of baselines that should not be used.\n";
  } else {
    bool keepIntra = false, keepCS013 = false;
    int argi = 1;
    std::vector<std::string> stations;
    while (argv[argi][0] == '-') {
      std::string p(&argv[argi][1]);
      if (p == "keep-intra") {
        keepIntra = true;
      } else if (p == "keep-cs013") {
        keepCS013 = true;
      } else if (p == "station") {
        ++argi;
        stations.push_back(argv[argi]);
      } else
        throw std::runtime_error("Unknown parameter");
      ++argi;
    }
    std::string msFilename(argv[argi]);

    std::set<size_t> flaggedAntenna;
    std::set<std::pair<size_t, size_t> > flaggedBaselines;

    MeasurementSet ms(msFilename, Table::Update);

    MSAntenna antennaTable(ms.antenna());
    ROScalarColumn<casacore::String> nameCol(
        antennaTable, MSAntenna::columnName(MSAntennaEnums::NAME));
    std::map<std::string, size_t> antennaNames;
    for (size_t i = 0; i != antennaTable.nrow(); ++i) {
      std::string name = nameCol(i);
      auto flagListIter = std::find(stations.begin(), stations.end(), name);
      if (!keepCS013 && name == "CS013HBA0")
        flaggedAntenna.insert(i);
      else if (!keepCS013 && name == "CS013HBA1")
        flaggedAntenna.insert(i);
      else if (flagListIter != stations.end()) {
        flaggedAntenna.insert(i);
        stations.erase(flagListIter);
      } else {
        std::string country;
        if (name.size() >= 2) country = name.substr(0, 2);
        if (country == "DE" || country == "FR" || country == "SE" ||
            country == "UK" || country == "PL")
          flaggedAntenna.insert(i);
      }

      antennaNames.insert(std::make_pair(name, i));
      if (!keepIntra && name.size() >= 9) {
        std::string counterpart(name);
        if (name.substr(5, 4) == "HBA0")
          counterpart[8] = '1';
        else if (name.substr(5, 4) == "HBA1")
          counterpart[8] = '0';
        else
          counterpart = std::string();
        if (!counterpart.empty()) {
          if (antennaNames.find(counterpart) != antennaNames.end()) {
            size_t cIndex = antennaNames[counterpart];
            flaggedBaselines.insert(std::make_pair(cIndex, i));
          }
        }
      }
    }
    std::cout << "Flagging:\n- Antennae:";
    for (std::set<size_t>::const_iterator i = flaggedAntenna.begin();
         i != flaggedAntenna.end(); ++i)
      std::cout << nameCol(*i) << ' ';
    std::cout << "\n- Baselines:";
    for (std::set<std::pair<size_t, size_t> >::const_iterator i =
             flaggedBaselines.begin();
         i != flaggedBaselines.end(); ++i)
      std::cout << nameCol(i->first) << "x" << nameCol(i->second) << ' ';
    std::cout << '\n';

    /**
     * Read some meta data from the measurement set
     */
    MSSpectralWindow spwTable = ms.spectralWindow();
    size_t spwCount = spwTable.nrow();
    if (spwCount != 1)
      throw std::runtime_error("Set should have exactly one spectral window");

    ROScalarColumn<int> numChanCol(
        spwTable,
        MSSpectralWindow::columnName(MSSpectralWindowEnums::NUM_CHAN));
    size_t channelCount = numChanCol.get(0);
    if (channelCount == 0) throw std::runtime_error("No channels in set");

    ROScalarColumn<int> ant1Column(ms, ms.columnName(MSMainEnums::ANTENNA1));
    ROScalarColumn<int> ant2Column(ms, ms.columnName(MSMainEnums::ANTENNA2));
    ArrayColumn<bool> flagsColumn(ms, ms.columnName(MSMainEnums::FLAG));

    if (ms.nrow() == 0) throw std::runtime_error("Table has no rows (no data)");
    IPosition flagsShape = flagsColumn.shape(0);
    unsigned polarizationCount = flagsShape[0];

    std::cout << "Flagging... " << std::flush;

    /**
     * Flag
     */
    Array<bool> flags(flagsShape);
    Array<bool>::contiter flagPtr = flags.cbegin();
    for (size_t ch = 0; ch != channelCount; ++ch) {
      for (size_t p = 0; p != polarizationCount; ++p) {
        *flagPtr = true;
        ++flagPtr;
      }
    }

    size_t crossCount = 0, autoCount = 0;
    for (size_t rowIndex = 0; rowIndex != ms.nrow(); ++rowIndex) {
      size_t a1 = ant1Column.get(rowIndex), a2 = ant2Column.get(rowIndex);

      bool isAntennaSelected =
          flaggedAntenna.find(a1) != flaggedAntenna.end() ||
          flaggedAntenna.find(a2) != flaggedAntenna.end();
      bool isBaselineSelected = flaggedBaselines.find(std::make_pair(a1, a2)) !=
                                    flaggedBaselines.end() ||
                                flaggedBaselines.find(std::make_pair(a2, a1)) !=
                                    flaggedBaselines.end();
      // Selected?
      if (isAntennaSelected || isBaselineSelected) {
        if (a1 == a2)
          ++autoCount;
        else
          ++crossCount;
        flagsColumn.put(rowIndex, flags);
      }
    }

    std::cout << "DONE (selected " << crossCount << " cross- and " << autoCount
              << " auto-correlated timesteps)\n";
  }

  return 0;
}
