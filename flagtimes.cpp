#include <iostream>
#include <stdexcept>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/tables/Tables/ScalarColumn.h>

using namespace casacore;

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout << "Usage: flagtimes <ms> <startindex> <endindex>\n"
                 "Flags all timesteps from startindex to endindex (startindex "
                 "is inclusive, endindex is excludive).\n";
  } else {
    size_t argi = 1;
    std::string msFilename(argv[argi]);
    size_t start = atoi(argv[argi + 1]);
    size_t end = atoi(argv[argi + 2]);

    MeasurementSet ms(msFilename, Table::Update);

    /**
     * Read some meta data from the measurement set
     */
    MSSpectralWindow spwTable = ms.spectralWindow();
    size_t spwCount = spwTable.nrow();
    if (spwCount != 1)
      throw std::runtime_error("Set should have exactly one spectral window");

    ROScalarColumn<int> numChanCol(
        spwTable,
        MSSpectralWindow::columnName(MSSpectralWindowEnums::NUM_CHAN));
    size_t channelCount = numChanCol.get(0);
    if (channelCount == 0) throw std::runtime_error("No channels in set");

    ROScalarColumn<double> timeColumn(ms, ms.columnName(MSMainEnums::TIME));
    ArrayColumn<bool> flagsColumn(ms, ms.columnName(MSMainEnums::FLAG));

    if (ms.nrow() == 0) throw std::runtime_error("Table has no rows (no data)");
    IPosition flagsShape = flagsColumn.shape(0);
    unsigned polarizationCount = flagsShape[0];

    std::cout << "Flagging... " << std::flush;

    /**
     * Flag
     */
    Array<bool> flags(flagsShape);
    Array<bool>::contiter flagPtr = flags.cbegin();
    for (size_t ch = 0; ch != channelCount; ++ch) {
      for (size_t p = 0; p != polarizationCount; ++p) {
        *flagPtr = true;
        ++flagPtr;
      }
    }

    size_t rowCount = 0;
    double lastTime = timeColumn.get(0);
    size_t timeIndex = 0;
    for (size_t rowIndex = 0; rowIndex != ms.nrow(); ++rowIndex) {
      double t = timeColumn.get(rowIndex);
      if (lastTime != t) {
        lastTime = t;
        ++timeIndex;
      }

      // Selected?
      if (timeIndex >= start && timeIndex < end) {
        ++rowCount;
        flagsColumn.put(rowIndex, flags);
      }
    }

    std::cout << "DONE (selected " << rowCount << " rows)\n";
  }

  return 0;
}
