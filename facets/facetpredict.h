#ifndef FACET_PREDICT_H
#define FACET_PREDICT_H

#include "ds9facetfile.h"
#include "facetimage.h"

#include <idg-api.h>

#include "../idg/idgconfiguration.h"

#include "../fftresampler.h"
#include "../fitsreader.h"
#include "../image.h"
#include "../system.h"

#include "../wsclean/smallinversionoptimization.h"
#include "../wsclean/wstackinggridder.h"

#include <algorithm>

class FacetPredict {
 public:
  FacetPredict(const std::string& fitsModelFile,
               const std::string& ds9RegionsFile)
      : _reader(fitsModelFile), _padding(1.0), _smallInversion(false) {
    Logger::SetVerbosity(Logger::VerboseVerbosity);

    DS9FacetFile f(ds9RegionsFile);
    const size_t width = _reader.ImageWidth(), height = _reader.ImageHeight();
    aocommon::UVector<double> model(width * height);
    _reader.Read(model.data());
    double maxBaseline = 40000.0;
    _fullWidth = width;
    _fullHeight = height;
    _pixelSizeX = _reader.PixelSizeX();
    _pixelSizeY = _reader.PixelSizeY();

    if (_smallInversion) {
      size_t optWidth, optHeight, minWidth, minHeight;
      double theoreticalBeamSize = 1.0 / maxBaseline;
      SmallInversionOptimization::DetermineOptimalSize(
          _fullWidth, _pixelSizeX, theoreticalBeamSize, minWidth, optWidth);
      SmallInversionOptimization::DetermineOptimalSize(
          _fullHeight, _pixelSizeY, theoreticalBeamSize, minHeight, optHeight);
      if (optWidth < _fullWidth || optHeight < _fullHeight) {
        size_t newWidth = std::max(std::min(optWidth, _fullWidth), size_t(32));
        size_t newHeight =
            std::max(std::min(optHeight, _fullHeight), size_t(32));
        Logger::Info << "Minimal inversion size: " << minWidth << " x "
                     << minHeight << ", using optimal: " << newWidth << " x "
                     << newHeight << "\n";
        Logger::Info << "(Saves a factor of "
                     << double(width * height) / (newWidth * newHeight)
                     << ")\n";
        _pixelSizeX = (double(_fullWidth) * _pixelSizeX) / double(newWidth);
        _pixelSizeY = (double(_fullHeight) * _pixelSizeY) / double(newHeight);
        _fullWidth = newWidth;
        _fullHeight = newHeight;

        Logger::Info << "Resampling image...\n";
        // Decimate the image
        // Input is ImageWidth() x ImageHeight()
        FFTResampler resampler(width, height, _fullWidth, _fullHeight, 1);

        aocommon::UVector<double> resampled(_fullWidth * _fullHeight);
        resampler.Resample(model.data(), resampled.data());
        model = std::move(resampled);
      } else {
        Logger::Info
            << "Small inversion enabled, but inversion resolution already "
               "smaller than beam size: not using optimization.\n";
      }
    }

    FacetMap map;
    f.Read(map, _reader.PhaseCentreRA(), _reader.PhaseCentreDec(), _pixelSizeX,
           _pixelSizeY, _fullWidth, _fullHeight);
    std::cout << "Read " << map.NFacets() << " facet definitions.\n";

    bool makeSquare = true;  // only necessary for IDG though
    size_t area = 0;
    for (size_t i = 0; i != map.NFacets(); ++i) {
      const Facet& facet = map[i];
      _images.emplace_back();
      FacetImage& image = _images.back();
      image.CopyFacetPart(facet, model.data(), _fullWidth, _fullHeight,
                          _padding, makeSquare);
      area += image.Width() * image.Height();
    }
    std::cout << "Area covered: " << area / 1024 << " Kpixels^2\n";
  }

  void SetMSInfo(double maxW, std::vector<std::vector<double>>&& bands,
                 size_t nr_stations, double max_baseline) {
    _maxW = maxW;
    _bands = std::move(bands);
    _nr_stations = nr_stations;
    _max_baseline = max_baseline;
  }

  void StartIDG() {
    _buffersets.clear();
    idg::api::Type proxyType = idg::api::Type::CPU_OPTIMIZED;
    int buffersize = 256;
    idg::api::options_type options;
    IdgConfiguration::Read(proxyType, buffersize, options);
    aocommon::UVector<double> data;
    for (FacetImage& img : _images) {
      double dl = (img.OffsetX() + int(img.Width() / 2) -
                   int(_reader.ImageWidth() / 2)) *
                  _pixelSizeX,
             dm = (img.OffsetY() + int(img.Height() / 2) -
                   int(_reader.ImageHeight() / 2)) *
                  _pixelSizeY;
      std::cout << "Initializing gridder " << _buffersets.size() << " ("
                << img.Width() << " x " << img.Height() << ", +"
                << img.OffsetX() << "," << img.OffsetY()
                << ", dl=" << dl * 180.0 / M_PI
                << " deg, dm=" << dm * 180.0 / M_PI << " deg)\n";

      data.assign(img.Width() * img.Height() * 4, 0.0);
      // TODO make full polarization
      std::copy(img.Data(), img.Data() + img.Width() * img.Height(),
                data.data());
      _buffersets.emplace_back(idg::api::BufferSet::create(proxyType));
      idg::api::BufferSet& bs = *_buffersets.back();
      options["padded_size"] = size_t(1.2 * img.Width());
      // options["max_threads"] = int(1);
      bs.init(img.Width(), _pixelSizeX, _maxW + 1.0, dl, dm, 0, options);
      bs.set_image(data.data());
      bs.init_buffers(buffersize, _bands, _nr_stations, _max_baseline, options,
                      idg::api::BufferSetType::degridding);
    }
  }

  void StartWS() {
    size_t threadCount = System::ProcessorCount();
    size_t kernelSize = 7;
    size_t overSamplingFactor = 63;
    const double pixelSizeX = _reader.PixelSizeX(),
                 pixelSizeY = _reader.PixelSizeY();
    std::vector<std::unique_ptr<WStackingGridder>> gridders;
    long mem = System::TotalMemory();
    double minW = 0.0, maxW = 11000.0;
    double area = 0.0;
    for (FacetImage& img : _images) {
      gridders.emplace_back(new WStackingGridder(
          img.Width(), img.Height(), pixelSizeX, pixelSizeY, threadCount,
          kernelSize, overSamplingFactor));
      double dl = (img.OffsetX() + int(img.Width() / 2) -
                   int(_reader.ImageWidth() / 2)) *
                  pixelSizeX,
             dm = (img.OffsetY() + int(img.Height() / 2) -
                   int(_reader.ImageHeight() / 2)) *
                  pixelSizeY;
      std::cout << "Initializing gridder " << gridders.size() << " ("
                << img.Width() << " x " << img.Height() << ", +"
                << img.OffsetX() << "," << img.OffsetY() << ", dl=" << dl
                << ", dm=" << dm << ")\n";
      double wWidth = img.Width() / _padding, wHeight = img.Height() / _padding;
      double maxL = wWidth * pixelSizeX * 0.5 + fabs(dl),
             maxM = wHeight * pixelSizeY * 0.5 + fabs(dm),
             dlmSq = dl * dl + dm * dm, lmSq = maxL * maxL + maxM * maxM;
      double radiansForAllLayers;
      radiansForAllLayers =
          2 * M_PI * (maxW - minW) * fabs(sqrt(1.0 - lmSq) - sqrt(1.0 - dlmSq));
      /*if(lmSq < 1.0)
              radiansForAllLayers = 2 * M_PI * (maxW - minW) * (1.0 - sqrt(1.0 -
      lmSq)); else radiansForAllLayers = 2 * M_PI * (maxW - minW);*/
      size_t nwLayers = size_t(ceil(radiansForAllLayers));
      WStackingGridder& gridder = *gridders.back();
      gridder.SetDenormalPhaseCentre(dl, dm);
      gridder.PrepareWLayers(nwLayers, mem, minW, maxW);
      area += nwLayers * img.Width() * img.Height();
      gridder.InitializePrediction(img.AcquireDataPtr());
      std::cout << "Area factor: "
                << area / (_reader.ImageWidth() * _reader.ImageHeight())
                << "\n";
      if (gridder.NPasses() != 1)
        throw std::runtime_error("Doesn't fit in memory.");
      gridder.StartPredictionPass(0);
    }
  }

  void RequestPredict(size_t direction, size_t dataDescId, size_t rowId,
                      size_t timeIndex, size_t antenna1, size_t antenna2,
                      const double* uvw) {
    idg::api::BufferSet& bs = *_buffersets[direction];
    while (
        bs.get_degridder(dataDescId)
            ->request_visibilities(rowId, timeIndex, antenna1, antenna2, uvw)) {
      computePredictionBuffer(dataDescId, bs);
    }
  }

  void (*PredictCallback)(size_t row, size_t dataDescId,
                          const std::complex<float>* values);

  size_t NDirections() const { return _images.size(); }

 private:
  void computePredictionBuffer(size_t dataDescId, idg::api::BufferSet& bs) {
    auto available_row_ids = bs.get_degridder(dataDescId)->compute();
    for (auto i : available_row_ids) {
      size_t row = i.first;
      const std::complex<float>* values = i.second;
      PredictCallback(row, dataDescId, values);
    }
    bs.get_degridder(dataDescId)->finished_reading();
  }

  std::vector<FacetImage> _images;
  std::vector<std::unique_ptr<idg::api::BufferSet>> _buffersets;

  size_t _fullWidth, _fullHeight;
  double _pixelSizeX, _pixelSizeY;
  FitsReader _reader;
  double _padding;
  bool _smallInversion;

  // MS info
  double _maxW;
  std::vector<std::vector<double>> _bands;
  size_t _nr_stations;
  double _max_baseline;
};

#endif
