#include "facetpredict.h"

#include "../multibanddata.h"
#include "../progressbar.h"

#include <casacore/tables/Tables/ArrayColumn.h>

void callback(size_t /*row*/, size_t /*dataDescId*/,
              const std::complex<float>* /*values*/) {}

int main(int argc, char* argv[]) {
  if (argc < 2)
    std::cout << "Syntax: testfpredict <fitsmodel> <regions> <ms>\n";
  else {
    const char* fitsModelFile = argv[1];
    const char* ds9RegionsFile = argv[2];
    const char* msFile = argv[3];
    FacetPredict predict(fitsModelFile, ds9RegionsFile);

    double maxW = 11000.0;
    casacore::MeasurementSet ms(msFile);
    MultiBandData bandData(ms.spectralWindow(), ms.dataDescription());
    std::vector<std::vector<double>> bands;
    for (const BandData& band : bandData) {
      bands.emplace_back();
      for (double channel : band) bands.back().emplace_back(channel);
    }
    size_t nr_stations = ms.antenna().nrow();
    std::cout << nr_stations << " stations.\n";
    double max_baseline = 80000.0;

    predict.SetMSInfo(maxW, std::move(bands), nr_stations, max_baseline);

    predict.StartIDG();

    casacore::Vector<double> uvw(3);
    casacore::ArrayColumn<double> uvwCol(
        ms, casacore::MS::columnName(casacore::MS::UVW));
    casacore::ScalarColumn<int> ant1Col(
        ms, casacore::MS::columnName(casacore::MS::ANTENNA1)),
        ant2Col(ms, casacore::MS::columnName(casacore::MS::ANTENNA2)),
        dataDescIdCol(ms, casacore::MS::columnName(casacore::MS::DATA_DESC_ID));
    casacore::ScalarColumn<double> timeCol(
        ms, casacore::MS::columnName(casacore::MS::TIME));

    size_t timeIndex = 0;
    double prevTime = -1.0;
    ProgressBar progress("Predicting");
    for (size_t row = 0; row != ms.nrow(); ++row) {
      uvwCol.get(row, uvw);
      int a1 = ant1Col(row), a2 = ant2Col(row), dataDescId = dataDescIdCol(row);
      double time = timeCol(row);
      if (time != prevTime) {
        ++timeIndex;
        prevTime = time;
      }

      predict.PredictCallback = &callback;
      for (size_t dir = 0; dir != predict.NDirections(); ++dir) {
        predict.RequestPredict(dir, dataDescId, row, timeIndex, a1, a2,
                               uvw.data());
      }
      progress.SetProgress(row, ms.nrow());
    }
  }
}
