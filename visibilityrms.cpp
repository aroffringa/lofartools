#include <cmath>
#include <iostream>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>

using namespace casacore;

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout
        << "Usage: visibilityrms <ms>\n"
           "Flags all correlations of baselines that should not be used.\n";
  } else {
    MeasurementSet ms(argv[1]);
    if (ms.nrow() == 0) throw std::runtime_error("Table has no rows (no data)");

    ArrayColumn<casacore::Complex> dataColumn(ms, "DATA");
    ArrayColumn<bool> flagColumn(ms, "FLAG");
    ArrayColumn<float> weightColumn(ms, "WEIGHT_SPECTRUM");
    IPosition dataShape = dataColumn.shape(0);
    Array<Complex> dataArray(dataShape);
    Array<bool> flagArray(dataShape);
    Array<float> weightArray(dataShape);

    double rms_sum = 0.0;
    double weight_sum = 0.0;

    for (size_t rowIndex = 0; rowIndex != ms.nrow(); ++rowIndex) {
      dataColumn.get(rowIndex, dataArray);
      flagColumn.get(rowIndex, flagArray);
      weightColumn.get(rowIndex, weightArray);

      Array<bool>::const_contiter flag_iter = flagArray.cbegin();
      Array<float>::const_contiter weight_iter = weightArray.cbegin();
      for (Array<Complex>::const_contiter value_iter = dataArray.cbegin();
           value_iter != dataArray.cend(); ++value_iter) {
        if (!*flag_iter) {
          const float weight = *weight_iter;
          const Complex value = *value_iter;
          rms_sum += std::norm(value) * weight;
          weight_sum += weight;
        }
        ++weight_iter;
        ++flag_iter;
      }
    }
    std::cout << std::sqrt(rms_sum / weight_sum) << '\n';
  }

  return 0;
}
