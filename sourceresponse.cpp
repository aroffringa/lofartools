#include <iostream>
#include <fstream>

#include "model/model.h"

#include <casacore/measures/TableMeasures/ArrayMeasColumn.h>
#include <casacore/measures/Measures/MEpoch.h>

#include <EveryBeam/coords/itrfconverter.h>
#include <EveryBeam/load.h>
#include <EveryBeam/pointresponse/pointresponse.h>

#include <aocommon/banddata.h>
#include <aocommon/matrix2x2.h>

using aocommon::MC2x2F;

void sourceResponse(const std::string& msFilename, const ModelComponent& source,
                    const std::string& filename) {
  casacore::MeasurementSet ms(msFilename);

  aocommon::BandData band(ms.spectralWindow());
  double subbandFrequency = band.CentreFrequency();

  casacore::MSField fieldTable(ms.field());
  if (fieldTable.nrow() != 1)
    throw std::runtime_error("Set has multiple fields");
  casacore::ScalarMeasColumn<casacore::MDirection> delayDirColumn(
      fieldTable,
      casacore::MSField::columnName(casacore::MSFieldEnums::DELAY_DIR));
  casacore::MDirection delayDir = delayDirColumn(0);

  casacore::MDirection tileBeamDir;
  if (fieldTable.tableDesc().isColumn("LOFAR_TILE_BEAM_DIR")) {
    casacore::ArrayMeasColumn<casacore::MDirection> tileBeamDirColumn(
        fieldTable, "LOFAR_TILE_BEAM_DIR");
    tileBeamDir = *(tileBeamDirColumn(0).data());
  } else {
    tileBeamDir = delayDir;
  }
  everybeam::Options options;
  std::unique_ptr<everybeam::telescope::Telescope> telescope =
      everybeam::Load(ms, options);

  casacore::MEpoch::ScalarColumn timeColumn(
      ms, ms.columnName(casacore::MSMainEnums::TIME));
  double startTime = timeColumn(0).getValue().get() * 86400.0;
  double prevTime = startTime - 1.0;

  std::ofstream file(filename);
  for (size_t row = 0; row != ms.nrow(); ++row) {
    casacore::MEpoch time = timeColumn(row);
    double timeAsDouble = time.getValue().get() * 86400.0;
    if (timeAsDouble != prevTime) {
      prevTime = timeAsDouble;
      everybeam::coords::ItrfConverter itrfConverter(timeAsDouble);

      double stokesI = source.SED().FluxAtFrequency(
          subbandFrequency, aocommon::Polarization::StokesI);

      MC2x2F response = MC2x2F::Zero();
      size_t count = 0;
      float maxEigenValue = 0.0;
      std::unique_ptr<everybeam::pointresponse::PointResponse> pointResponse =
          telescope->GetPointResponse(timeAsDouble);
      for (size_t station = 0; station != telescope->GetNrStations();
           ++station) {
        MC2x2F gainMatrix;
        pointResponse->Response(everybeam::BeamMode::kFull,
                                aocommon::DubiousComplexPointerCast(gainMatrix),
                                source.PosRA(), source.PosDec(),
                                subbandFrequency, station, 0);
        response += gainMatrix;
        ++count;
        std::complex<float> e1, e2;
        aocommon::Matrix2x2::EigenValues(
            aocommon::DubiousComplexPointerCast(gainMatrix * stokesI), e1, e2);
        maxEigenValue =
            std::max(maxEigenValue, std::max(std::abs(e1), std::abs(e2)));
      }
      response = response * (1.0 / count);
      std::complex<float> e1, e2;
      aocommon::Matrix2x2::EigenValues(
          aocommon::DubiousComplexPointerCast(response * stokesI), e1, e2);
      file << ((timeAsDouble - startTime) / 3600.0) << '\t' << maxEigenValue
           << '\t' << std::max(std::abs(e1), std::abs(e2)) << '\n';
    }
  }
}

void header(std::unique_ptr<std::ofstream>& responsePlt,
            const std::string& name) {
  responsePlt.reset(new std::ofstream(name + ".plt"));
  *responsePlt << "set terminal pdfcairo enhanced color font 'Times,16'\n"
                  "#set logscale xy\n"
                  "#set yrange [0.1:]\n"
                  "set output \""
               << name
               << ".pdf\"\n"
                  "set key above\n"
                  "set xlabel \"Time (h)\"\n"
                  "set ylabel \"Apparent flux (Jy)\"\n"
                  "plot \\\n";
}

int main(int argc, char* argv[]) {
  if (argc < 3) std::cout << "Syntax: sourceresponse <ms> <model>\n";

  std::unique_ptr<std::ofstream> maxPlt, avgPlt;
  header(maxPlt, "response-max");
  header(avgPlt, "response-avg");

  Model model(argv[2]);
  bool isFirst = true;
  for (const ModelSource& s : model) {
    for (size_t i = 0; i != s.ComponentCount(); ++i) {
      const ModelComponent& c = s.Component(i);
      std::string name = (s.ComponentCount() != 1)
                             ? s.Name() + "_" + std::to_string(i)
                             : s.Name();
      std::cout << "Calculating " << name << "...\n";
      sourceResponse(argv[1], c, name + ".txt");
      if (!isFirst) {
        *maxPlt << ",\\\n";
        *avgPlt << ",\\\n";
      } else {
        isFirst = false;
      }
      *maxPlt << '"' << name << ".txt\" using 1:2 with lines title '" << name
              << "' lw 2";
      *avgPlt << '"' << name << ".txt\" using 1:3 with lines title '" << name
              << "' lw 2";
    }
  }
  *maxPlt << "\n";
  *avgPlt << "\n";
}
