#include <iostream>

#include <aocommon/uvector.h>
#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>
#include <aocommon/matrix2x2.h>
#include <aocommon/hmatrix4x4.h>

using aocommon::FitsReader;
using aocommon::FitsWriter;
using aocommon::MC2x2;
using aocommon::Vector4;

void readJones(const std::string& beamPrefix) {
  FitsReader xxrReader(beamPrefix + "-XX.fits"),
      xxiReader(beamPrefix + "-XXi.fits"), xyrReader(beamPrefix + "-XY.fits"),
      xyiReader(beamPrefix + "-XYi.fits"), yxrReader(beamPrefix + "-YX.fits"),
      yxiReader(beamPrefix + "-YXi.fits"), yyrReader(beamPrefix + "-YY.fits"),
      yyiReader(beamPrefix + "-YYi.fits");
  FitsReader* readers[8];
  readers[0] = &xxrReader;
  readers[1] = &xxiReader;
  readers[2] = &xyrReader;
  readers[3] = &xyiReader;
  readers[4] = &yxrReader;
  readers[5] = &yxiReader;
  readers[6] = &yyrReader;
  readers[7] = &yyiReader;
  aocommon::UVector<double> images[8];
  const size_t size = xxrReader.ImageWidth() * xxrReader.ImageHeight();
  aocommon::UVector<double> avgpbImage(size);
  for (size_t i = 0; i != 8; ++i) {
    images[i].resize(size);
    readers[i]->Read(images[i].data());
  }
  for (size_t i = 0; i != size; ++i) {
    std::complex<double> xx(images[0][i], images[1][i]),
        xy(images[2][i], images[3][i]), yx(images[4][i], images[5][i]),
        yy(images[6][i], images[7][i]);
    MC2x2 beam(xx, xy, yx, yy);
    beam = beam.MultiplyHerm(beam);
    avgpbImage[i] = 0.5 * (beam.Get(0).real() + beam.Get(3).real());
  }
  FitsWriter writer(xxrReader);
  writer.SetPolarization(aocommon::Polarization::StokesI);
  writer.Write(beamPrefix + "-avgpb.fits", &avgpbImage[0]);
}

void readMueller(const std::string& beamPrefix) {
  size_t width = 0, height = 0;
  aocommon::UVector<double> img;
  std::vector<aocommon::HMC4x4> matrices;
  std::unique_ptr<aocommon::FitsReader> reader;
  for (size_t el = 0; el != 16; ++el) {
    reader.reset(new aocommon::FitsReader(beamPrefix + "-" +
                                          std::to_string(el) + ".fits"));
    if (width == 0) {
      width = reader->ImageWidth();
      height = reader->ImageHeight();
      img.resize(width * height);
      matrices.resize(width * height);
    }
    reader->Read(img.data());
    for (size_t i = 0; i != img.size(); ++i) {
      matrices[i].Data(el) = img[i];
    }
  }

  const Vector4 vec{0.5, 0.0, 0.0, 0.5};
  for (size_t i = 0; i != img.size(); ++i) {
    Vector4 res = matrices[i] * vec;
    img[i] = res[0].real() + res[3].real();
  }

  FitsWriter writer(*reader);
  writer.SetPolarization(aocommon::Polarization::StokesI);
  writer.Write(beamPrefix + "-avgpb.fits", img.data());
}

int main(int argc, char* argv[]) {
  if (argc != 2) {
    std::cout << "Syntax: makeavgpb <beam prefix>\n";
  } else {
    try {
      readJones(argv[1]);
    } catch (std::exception& e) {
      readMueller(argv[1]);
    }
  }
}
