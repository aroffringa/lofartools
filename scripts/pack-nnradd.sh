rm -rf tmp
rm -rfv nnradd.tar.bz2

curpwd=`pwd`
mkdir -p tmp
cp -vr ../nnradd.cpp ../angle.* ../fitswriter.* ../fitsreader.* ../fitsiochecker.* ../imagecoordinates.* ../polarizationenum.* ../CMake tmp/
cp -v ../aocommon/uvector_03.h tmp/uvector.h
cp CMakeLists-nnradd.txt tmp/CMakeLists.txt
cd tmp
tar -cjvf ${curpwd}/nnradd.tar.bz2 CMake CMakeLists.txt nnradd.cpp angle.* fitswriter.* fitsreader.* fitsiochecker.* imagecoordinates.* polarizationenum.* uvector.h
cd ${curpwd}
rm -rf tmp

mkdir tmp
cd tmp
tar -xjvf ${curpwd}/nnradd.tar.bz2
mkdir build
cd build
cmake ../
make
cd ${curpwd}
