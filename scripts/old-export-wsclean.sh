#! /bin/bash
pars="-v"
if [[ $1 == "" ]] ; then
		echo Syntax: $0 \<dir\>
else
		WORKDIR=`pwd`
		DEST="$1"
		cd ..
		cp ${pars} application.* areaset.* banddata.* buffered_lane.* casamaskreader.* dftpredictionalgorithm.* fftconvolver.* fftresampler.* fftwmanager.* fitsiochecker.* fitsreader.* fitswriter.* gaussianfitter.* hmatrix4x4.* image.* imageweights.* multibanddata.* modelrenderer.* msselection.* ndppp.* nlplfitter.* numberlist.* parsetreader.* polynomialfitter.* polynomialchannelfitter.* progressbar.* rmsimage.* serializable.* serial?stream.* stopwatch.* system.* threadpool.* weightmode.* windowfunction.* wscleanmain.cpp wscversion.h.in CMakeVersionInfo.txt Doxyfile.in ${DEST}/
		cp ${pars} aocommon/include/aocommon/*.{h,cpp} ${DEST}/aocommon/include/aocommon
		cp ${pars} aterms/*.{h,cpp} ${DEST}/aterms/
		cp ${pars} deconvolution/*.{h,cpp} ${DEST}/deconvolution/
		cp ${pars} distributed/*.{h,cpp} ${DEST}/distributed
		cp ${pars} idg/*.{h,cpp} ${DEST}/idg
		cp ${pars} interface/*.{c,h,cpp,py} ${DEST}/interface
		cp ${pars} iuwt/*.{h,cpp} ${DEST}/iuwt/
		cp ${pars} lofar/{lbeamevaluator.*,lmspredicter.*,lbeamimagemaker.*,lofarbeamkeywords.*} ${DEST}/lofar/
		cp ${pars} model/{bbsmodel.*,modelparser.*,tokenizer.*,measurement.*,measuredsed.*,model.*,modelcomponent.*,modelsource.*,powerlawsed.*,spectralenergydistribution.*} ${DEST}/model/
		cp ${pars} msproviders/*.{h,cpp} ${DEST}/msproviders/
		cp ${pars} multiscale/*.{h,cpp} ${DEST}/multiscale/
		cp ${pars} mwa/*.{h,cpp} ${DEST}/mwa/
		cp ${pars} primarybeam/*.{h,cpp} ${DEST}/primarybeam
		cp ${pars} scheduling/*.{h,cpp} ${DEST}/scheduling
		cp ${pars} tests/*.cpp ${DEST}/tests/
		cp ${pars} units/*.h ${DEST}/units/
		cp ${pars} wsclean/*.{h,cpp} ${DEST}/wsclean/
		cp ${pars} wgridder/*.{h,cpp} ${DEST}/wgridder/
		cp ${pars} wsclean/examples/*.cpp ${DEST}/wsclean/examples/
		cp ${pars} wsclean/examples/Makefile ${DEST}/wsclean/examples/
#		diff idg/idgmsgridder.cpp ${DEST}/idg/idgmsgridder.cpp
#		diff idg/idgmsgridder.h ${DEST}/idg/idgmsgridder.h
		cat ${DEST}/CMakeVersionInfo.txt
fi
