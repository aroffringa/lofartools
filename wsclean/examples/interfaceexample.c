#include "../../interface/wscleaninterface.h"

#include "math.h"

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	if(argc < 2)
	{
		printf("Specify measurement set: interfaceexample <ms>\n");
	}
	else {
		imaging_parameters parameters;
		imaging_data imgData;

		void* userdata;
		parameters.msPath = argv[1];
		parameters.imageWidth = 512;
		parameters.imageHeight = 512;
		parameters.pixelScaleX = 2.0 * M_PI/(180.0*60.0); // 2 amin
		parameters.pixelScaleY = 2.0 * M_PI/(180.0*60.0);
		parameters.extraParameters="-weight natural";
		
		wsclean_initialize(&userdata, &parameters, &imgData);
		
		complex double* mydata = (complex double*) malloc(imgData.dataSize * sizeof(complex double));
		complex double* emptydata = (complex double*) malloc(imgData.dataSize * sizeof(complex double));
		double* myweights = (double*) malloc(imgData.dataSize * sizeof(double));
		double* myimage = (double*) malloc(parameters.imageWidth*parameters.imageHeight * sizeof(double));
		
		wsclean_operator_At(userdata, myimage, emptydata);
		wsclean_operator_A(userdata, mydata, myimage);
		
		wsclean_read(userdata, mydata, myweights);
		wsclean_operator_At(userdata, myimage, mydata);
		wsclean_operator_A(userdata, mydata, myimage);
		
		size_t i;
		for(i=0; i!=imgData.dataSize; ++i)
			mydata[i] = 1.0;
		
		wsclean_operator_At(userdata, myimage, mydata);
		wsclean_operator_A(userdata, mydata, myimage);
		
		wsclean_write(userdata, "wsclean-interface-test.fits", myimage);
		
		free(myimage);
		free(mydata);
		
		wsclean_deinitialize(userdata);
	}
}
