#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

#include <aocommon/fits/fitsreader.h>

#include "colormap.h"
#include <aocommon/image.h>
#include "pngfile.h"

using namespace std;
using namespace aocommon;

struct ImageInfo {
  unsigned index;
  long double variance;
};

bool operator<(const ImageInfo &a, const ImageInfo &b) {
  return a.variance >
         b.variance;  // note that a noise image is "smaller" than a clean image
}

double RankToValue(DImage &image, size_t count) {
  const size_t size = image.Width() * image.Height();
  std::vector<double> sorted(image.begin(), image.end());
  std::sort(sorted.begin(), sorted.end());
  return sorted[size - count - 1];
}

void addFits(DImage &red, DImage &green, DImage &blue, DImage &mono,
             const std::string &filename);

void HLStoRGB(long double hue, long double lum, long double sat,
              long double &red, long double &green, long double &blue);
void WLtoRGB(long double wavelength, long double &red, long double &green,
             long double &blue);
inline void ScaledWLtoRGB(long double position, long double &red,
                          long double &green, long double &blue) {
  WLtoRGB(position * 300.0 + 400.0, red, green, blue);
  if (red < 0.0) red = 0.0;
  if (red > 1.0) red = 1.0;
  if (green < 0.0) green = 0.0;
  if (green > 1.0) green = 1.0;
  if (blue < 0.0) blue = 0.0;
  if (blue > 1.0) blue = 1.0;
}

int main(int argc, char *argv[]) {
  int pindex = 1;
  // parameters
  bool useSpectrum = true, blackWhite = false, mapColours = false;
  std::string colourMapName = "";
  std::unique_ptr<ColorMap> map;
  bool colormap = false;
  int removeNoiseImages = 0;
  enum ScaleMethod { MaximumContrast, Constant } scaleMethod = MaximumContrast;
  long double scaleValue = 1.0;
  std::string subtractFile, outputPngFile;
  bool subtract = false, redblue = false, individualMaximization = false,
       displayMax = false, singleImage = false;
  bool savePng = false;
  size_t singleImageIndex = 0;

  while (pindex < argc && argv[pindex][0] == '-') {
    string parameter = argv[pindex] + 1;
    if (parameter == "s") {
      useSpectrum = true;
    } else if (parameter == "bw") {
      blackWhite = true;
    } else if (parameter == "c") {
      useSpectrum = false;
    } else if (parameter == "d") {
      ++pindex;
      subtractFile = argv[pindex];
      subtract = true;
    } else if (parameter == "fi") {
      individualMaximization = true;
    } else if (parameter == "fm") {
      scaleMethod = MaximumContrast;
    } else if (parameter == "fv") {
      scaleMethod = Constant;
      ++pindex;
      scaleValue = atof(argv[pindex]);
    } else if (parameter == "m") {
      colormap = true;
    } else if (parameter == "max") {
      displayMax = true;
    } else if (parameter == "mc") {
      mapColours = true;
      ++pindex;
      colourMapName = argv[pindex];
    } else if (parameter == "png") {
      savePng = true;
      ++pindex;
      outputPngFile = argv[pindex];
    } else if (parameter == "r") {
      ++pindex;
      removeNoiseImages = atoi(argv[pindex]);
    } else if (parameter == "rb") {
      redblue = true;
    } else if (parameter == "si") {
      singleImage = true;
      ++pindex;
      singleImageIndex = atoi(argv[pindex]);
    } else {
      cerr << "Unknown parameter: -" << parameter << endl;
      return -1;
    }
    ++pindex;
  }

  if (argc - pindex < 1) {
    cerr
        << "Usage: \n\t" << argv[0]
        << " [options] <input fits file>\n"
           "\toptions:\n"
           "\t-d <fitsfile> subtract the file from the image\n"
           "\t-fi maximize each individual image before summing\n"
           "\t-fits <file> store in fits file (does not preserve the headers)\n"
           "\t-fm scale colors for maximum contrast, upper 0.02% of the data "
           "will be oversaturated (default)\n"
           "\t-fv <value> scale so that <value> flux is full brightness\n"
           "\t-m add colormap to image\n"
           "\t-max display maximum of each image\n"
           "\t-png <file> save as png file\n"
           "\t-rb don't use frequency colored, but use red/blue map for "
           "positive/negative values\n"
           "\t-s use spectrum (default)\n"
           "\t-si <index> select single image from each fits file\n"
           "\t-c use color circle\n";
    return -1;
  }

  std::unique_ptr<DImage> red, green, blue, mono;

  long double totalRed = 0.0, totalGreen = 0.0, totalBlue = 0.0;
  unsigned addedCount = 0;

  if (mapColours) {
    map = ColorMap::CreateColorMap(colourMapName);
  }

  size_t inputCount = argc - pindex;
  for (unsigned inputIndex = pindex; inputIndex < (unsigned)argc;
       ++inputIndex) {
    cout << "Opening " << argv[inputIndex] << "..." << endl;
    FitsReader fitsfile(argv[inputIndex]);
    size_t images = fitsfile.NFrequencies();
    size_t width = fitsfile.ImageWidth(), height = fitsfile.ImageHeight();

    std::unique_ptr<FitsReader> subtractFits;
    if (subtract) {
      cout << "Opening " << subtractFile << "..." << endl;
      subtractFits.reset(new FitsReader(subtractFile));
      unsigned sImages = fitsfile.NFrequencies();
      if (sImages < images) images = sImages;
    }

    std::vector<ImageInfo> variances;

    if (removeNoiseImages > 0) {
      cout << "Sorting images on noise level..." << endl;
      for (unsigned i = 0; i < images; ++i) {
        if (i % 8 == 0) {
          unsigned upper = i + 9;
          if (upper > images) upper = images;
          cout << "Measuring noise level in images " << (i + 1) << " - "
               << upper << "..." << endl;
        }
        DImage image(width, height);
        fitsfile.ReadIndex(image.Data(), i);
        struct ImageInfo imageInfo;
        imageInfo.variance = image.RMS();
        imageInfo.index = i;
        variances.push_back(imageInfo);
      }
      sort(variances.begin(), variances.end());

      cout << "The following images are removed because of too much noise: "
           << variances.front().index;
      for (std::vector<ImageInfo>::const_iterator i = variances.begin() + 1;
           i < variances.begin() + removeNoiseImages; ++i) {
        cout << ", " << i->index;
      }
      cout << endl;
    }

    unsigned lowI, highI;
    if (singleImage) {
      lowI = singleImageIndex;
      highI = singleImageIndex + 1;
    } else {
      lowI = 0;
      highI = images;
    }

    for (unsigned i = lowI; i < highI; ++i) {
      if (i % 8 == 0) {
        unsigned upper = i + 9;
        if (upper > images) upper = images;
        cout << "Adding image " << (i + 1) << " - " << upper << "..." << endl;
      }
      bool skip = false;
      if (removeNoiseImages > 0) {
        for (std::vector<ImageInfo>::const_iterator j = variances.begin();
             j < variances.begin() + removeNoiseImages; ++j) {
          if (j->index == i) {
            skip = true;
            break;
          }
        }
      }
      if (!skip) {
        long double wavelengthRatio;
        if (images * inputCount > 1)
          wavelengthRatio =
              (1.0 -
               (long double)(i + ((int)inputIndex - (int)pindex) * images) /
                   (images * inputCount - 1.0));
        else
          wavelengthRatio = 0.5;
        std::cout << "ratio=" << wavelengthRatio << '\n';
        DImage image(width, height);
        fitsfile.ReadIndex(image.Data(), i);
        if (subtract) {
          DImage imageB(width, height);
          subtractFits->ReadIndex(imageB.Data(), i);
          image -= imageB;
        }
        long double max;
        if (individualMaximization) {
          max = image.Max();
          if (max <= 0.0) max = 1.0;
        } else {
          max = 1.0;
        }
        if (displayMax)
          cout << "max=" << image.Min() << ":" << image.Max() << '\n';
        long double r = 0.0, g = 0.0, b = 0.0;
        if (blackWhite || mapColours) {
          r = 1.0;
          b = 1.0;
          g = 1.0;
        } else if (redblue) {
          r = 1.0;
          b = 1.0;
        } else if (useSpectrum)
          ScaledWLtoRGB(wavelengthRatio, r, g, b);
        else
          HLStoRGB(wavelengthRatio, 0.5, 1.0, r, g, b);
        totalRed += r;
        totalGreen += g;
        totalBlue += b;
        if (!red) {
          red.reset(new DImage(image.Width(), image.Height(), 0.0));
          green.reset(new DImage(image.Width(), image.Height(), 0.0));
          blue.reset(new DImage(image.Width(), image.Height(), 0.0));
          mono.reset(new DImage(image.Width(), image.Height(), 0.0));
        }
        size_t minY = image.Height(), minX = image.Width();
        if (red->Height() < minY) minY = red->Height();
        if (red->Width() < minX) minX = red->Width();
        for (unsigned y = 0; y < minY; ++y) {
          for (unsigned x = 0; x < minX; ++x) {
            long double value = image[x + y * width];
            (*mono)[x + y * width] += value;
            if (mapColours) {
              double mapVal = 2.0 * value / max / scaleValue - 1.0;
              if (mapVal < -1.0) mapVal = -1.0;
              if (mapVal > 1.0) mapVal = 1.0;
              (*red)[x + y * width] += map->ValueToColorR(mapVal) / 255.0;
              (*green)[x + y * width] += map->ValueToColorG(mapVal) / 255.0;
              (*blue)[x + y * width] += map->ValueToColorB(mapVal) / 255.0;
            } else if (blackWhite) {
              (*red)[x + y * width] += value / max;
              (*green)[x + y * width] += value / max;
              (*blue)[x + y * width] += value / max;
            } else if (redblue) {
              if (value > 0)
                (*red)[x + y * width] += value / max;
              else
                (*blue)[x + y * width] += value / (-max);
            } else {
              if (value < 0.0) value = 0.0;
              value /= max;
              if (colormap && (y < 96 && y >= 32 && x < images * 8)) {
                if (x >= i * 8 && x < i * 8 + 8) {
                  (*red)[x + y * width] = r * images;
                  (*green)[x + y * width] = g * images;
                  (*blue)[x + y * width] = b * images;
                }
              } else {
                (*red)[x + y * width] += r * value;
                (*green)[x + y * width] += g * value;
                (*blue)[x + y * width] += b * value;
              }
            }
          }
        }
        ++addedCount;
      }
    }
  }

  cout << "Scaling to ordinary units..." << endl;
  for (unsigned y = 0; y < red->Height(); ++y) {
    for (unsigned x = 0; x < red->Width(); ++x) {
      (*red)[x + y * red->Width()] /= addedCount;
      (*green)[x + y * red->Width()] /= addedCount;
      (*blue)[x + y * red->Width()] /= addedCount;
      (*mono)[x + y * red->Width()] /= addedCount;
    }
  }

  if (savePng) {
    cout << "Normalizing..." << endl;
    long double maxRed, maxGreen, maxBlue;
    switch (scaleMethod) {
      default:
      case MaximumContrast:
        maxRed = RankToValue(*red, red->Width() * red->Height() / 5000);
        maxGreen = RankToValue(*green, green->Width() * green->Height() / 5000);
        maxBlue = RankToValue(*blue, blue->Width() * blue->Height() / 5000);
        break;
      case Constant:
        if (!mapColours) {
          maxRed = scaleValue;
          maxGreen = scaleValue * totalGreen / totalRed;
          maxBlue = scaleValue * totalBlue / totalRed;
        } else {
          maxRed = 1.0;
          maxGreen = 1.0;
          maxBlue = 1.0;
        }
        break;
    }
    if (maxRed <= 0.0) maxRed = 1.0;
    if (maxGreen <= 0.0) maxGreen = 1.0;
    if (maxBlue <= 0.0) maxBlue = 1.0;
    cout << "Contrast stretch value for red: " << maxRed << endl;

    PngFile file(outputPngFile, red->Width(), red->Height());
    file.BeginWrite();

    cout << "Writing " << outputPngFile << "..." << endl;
    const size_t width = red->Width();
    for (unsigned y = 0; y < red->Height(); ++y) {
      for (unsigned x = 0; x < width; ++x) {
        unsigned r = (unsigned)(((*red)[x + y * width] / maxRed) * 255.0),
                 g = (unsigned)(((*green)[x + y * width] / maxGreen) * 255.0),
                 b = (unsigned)(((*blue)[x + y * width] / maxBlue) * 255.0);
        if (r > 255) r = 255;
        if (g > 255) g = 255;
        if (b > 255) b = 255;
        if ((*red)[x + y * width] < 0) r = 0;
        if ((*green)[x + y * width] < 0) g = 0;
        if ((*blue)[x + y * width] < 0) b = 0;
        file.PlotPixel(x, red->Height() - 1 - y, r, g, b, 255);
      }
    }
    file.Close();
  }

  return EXIT_SUCCESS;
}

/* utility routine for HLStoRGB */
long double HueToRGB(long double p, long double q, long double tc) {
  /* range check: note values passed add/subtract thirds of range */
  if (tc < 0) tc += 1.0;

  if (tc > 1.0) tc -= 1.0;

  /* return r,g, or b value from this tridrant */
  if (tc < (1.0 / 6.0)) return (p + (q - p) * 6.0 * tc);
  if (tc < 0.5) return (q);
  if (tc < 2.0 / 3.0)
    return (p + (q - p) * 6.0 * (2.0 / 3.0 - tc));
  else
    return (p);
}

void HLStoRGB(long double hue, long double lum, long double sat,
              long double &red, long double &green, long double &blue) {
  if (sat == 0) {
    red = green = blue = lum;
  } else {
    long double q, p;
    if (lum < 0.5)
      q = lum * (1.0 + sat);
    else
      q = lum + sat - (lum * sat);
    p = 2.0 * lum - q;

    red = HueToRGB(p, q, hue + 1.0 / 3.0);
    green = HueToRGB(p, q, hue);
    blue = HueToRGB(p, q, hue - 1.0 / 3.0);
  }
}

void WLtoRGB(long double wavelength, long double &red, long double &green,
             long double &blue) {
  if (wavelength >= 350.0 && wavelength <= 439.0) {
    red = -(wavelength - 440.0) / (440.0 - 350.0);
    green = 0.0;
    blue = 1.0;
  } else if (wavelength >= 440.0 && wavelength <= 489.0) {
    red = 0.0;
    green = (wavelength - 440.0) / (490.0 - 440.0);
    blue = 1.0;
  } else if (wavelength >= 490.0 && wavelength <= 509.0) {
    red = 0.0;
    green = 1.0;
    blue = -(wavelength - 510.0) / (510.0 - 490.0);
  } else if (wavelength >= 510.0 && wavelength <= 579.0) {
    red = (wavelength - 510.0) / (580.0 - 510.0);
    green = 1.0;
    blue = 0.0;
  } else if (wavelength >= 580.0 && wavelength <= 644.0) {
    red = 1.0;
    green = -(wavelength - 645.0) / (645.0 - 580.0);
    blue = 0.0;
  } else if (wavelength >= 645.0 && wavelength <= 780.0) {
    red = 1.0;
    green = 0.0;
    blue = 0.0;
  } else {
    red = 1.0;
    green = 0.0;
    blue = 0.0;
  }
  if (wavelength >= 350.0 && wavelength <= 419.0) {
    long double factor;
    factor = 0.3 + 0.7 * (wavelength - 350.0) / (420.0 - 350.0);
    red *= factor;
    green *= factor;
    blue *= factor;
  } else if (wavelength >= 420.0 && wavelength <= 700.0) {
    // nothing to be done
  } else if (wavelength >= 701.0 && wavelength <= 780.0) {
    long double factor;
    factor = 0.3 + 0.7 * (780.0 - wavelength) / (780.0 - 700.0);
    red *= factor;
    green *= factor;
    blue *= factor;
  } else if (wavelength >= 780.0) {
    long double factor;
    factor = 0.3;
    red *= factor;
    green *= factor;
    blue *= factor;
  } else {
    red = 0.0;
    green = 0.0;
    blue = 0.0;
  }
}
