#include "sagecalformat.h"

namespace model::sagecal {

namespace {
std::string getSagecalSourceName(const ModelSource& s, const ModelComponent& c,
                                 size_t compIndex) {
  std::stringstream str;
  switch (c.Type()) {
    case ModelComponent::PointSource:
      str << std::string("P_") << s.Name() << "_C" << compIndex;
      return str.str();
    case ModelComponent::GaussianSource:
      str << std::string("G_") << s.Name() << "_C" << compIndex;
      return str.str();
    default:
      throw std::runtime_error("Invalid component type");
  }
}

bool SkipLines(std::istream& stream) {
  do {
    const int first_char = stream.get();
    if (first_char == std::istream::traits_type::eof()) return true;
    if (first_char == '\n') continue;
    if ((first_char != '#')) {
      stream.putback(first_char);
      return false;
    } else {  // skip this line
      int next_char = stream.get();
      while (next_char != '\n') {
        if (next_char == std::istream::traits_type::eof()) return true;
        next_char = stream.get();
      };
    }
  } while (true);
}

void SkipRestOfLine(std::istream& stream) {
  int c = stream.get();
  while (c != '\n') {
    if (c == std::istream::traits_type::eof())
      throw std::runtime_error("Error reading Sagecal file");
    c = stream.get();
  }
}

// Reads a single contiguous string
// Returns true if line ends with a newline or eof, false when
// ends with a blank.
bool ReadNextString(std::vector<char>& buffer, std::istream& stream) {
  // Skip leading white space
  int c;
  do {
    c = stream.get();
    if (c == '\n' || c == '\r' || c == std::istream::traits_type::eof()) {
      buffer.assign(1, 0);
      return true;
    }
  } while (c != std::istream::traits_type::eof() && isblank(c));
  if (c == '\n' || c == '\r' || c == std::istream::traits_type::eof()) {
    buffer.assign(1, 0);
    return true;
  }
  buffer.clear();
  buffer.emplace_back(c);
  c = stream.get();
  bool new_line = false;
  while (c != std::istream::traits_type::eof()) {
    if (c == '\n' || c == '\r') {
      new_line = true;
      break;
    }
    if (isblank(c)) {
      break;
    } /* not end of line */
    buffer.emplace_back(c);
    c = stream.get();
  }
  // now c == blank , \n or eof
  // add '\0' to end
  buffer.emplace_back(0);
  return new_line;
}

}  // namespace

void Write(Model& model, std::ostream& sources_stream,
           std::ostream& cluster_stream, bool use_old_format,
           size_t first_cluster_size) {
  std::map<std::string, std::vector<const ModelSource*>> clusters;
  sources_stream << "## name h m s d m s I Q U V si0 si1 si2 RM eX(rad) "
                    "exY(rad) PA(rad) freq0\n";
  for (Model::const_iterator s = model.begin(); s != model.end(); ++s) {
    clusters[s->ClusterName()].push_back(&*s);

    for (size_t compIndex = 0; compIndex != s->ComponentCount(); ++compIndex) {
      const ModelComponent& c = s->Component(compIndex);
      const PowerLawSED* pl = dynamic_cast<const PowerLawSED*>(&c.SED());
      if (pl == nullptr)
        throw std::runtime_error("Expecting model with power law SEDs");
      long double ra = c.PosRA(), dec = c.PosDec();
      // I'm not sure this is necessary for Sagecal, but just for
      // sure convert negative ra's to positive ones.
      if (ra < 0.0) ra += 2.0 * M_PI;
      int raH, raM, decD, decM;
      double raS, decS;
      RaDecCoord::RAToHMS(ra, raH, raM, raS);
      RaDecCoord::DecToDMS(dec, decD, decM, decS);
      std::string cName = getSagecalSourceName(*s, c, compIndex);
      // Note that the sagecal axes are in radians.
      // Also, polarization angle is clockwise from North, while normal PA is
      // anti-clockwise from East.
      long double axMaj = c.MajorAxis();
      long double axMin = c.MinorAxis();
      long double axPA = -0.5L * M_PI + c.PositionAngle();
      double refFreq;
      double iquv[4];

      std::vector<double> si;
      pl->GetData(refFreq, iquv, si);
      if (si.size() > 3)
        throw std::runtime_error("Expecting 1-3 SI terms in model");
      while (si.size() < 3) si.push_back(0.0);

      if (use_old_format) {
        /*
         * See https://github.com/nlesc-dirac/sagecal/pull/168 .
         * The correction factor because of the change to ut^2 + vt^2 is:
         * axis <- axis * sqrt(2*pi*pi)
         * The correction factor for eX and eY is:
         * axis <- axis * 0.5 / (2*sqrt(2*log(2)))
         * Together:
         * axis <- axis * 0.5 * sqrt(2*pi*pi) / (2*sqrt(2*log(2)))
         *      <- axis * pi * sqrt(2) / (4 * sqrt(2*log(2)))
         *      <- axis * pi / (4 * sqrt(log(2)))
         */
        axMaj *= M_PI / (4.0 * (std::sqrt(M_LN2)));
        axMin *= M_PI / (4.0 * (std::sqrt(M_LN2)));
        for (double& value : iquv) value /= M_PI_2;
      }

      sources_stream << cName << ' ' << raH << ' ' << raM << ' ' << raS << ' '
                     << decD << ' ' << decM << ' ' << decS << ' ' << iquv[0]
                     << ' ' << iquv[1] << ' ' << iquv[2] << ' ' << iquv[3]
                     << ' ' << si[0] << ' ' << si[1] / M_LN10 << ' '
                     << si[2] / (M_LN10 * M_LN10) << ' ' << 0.0 << ' '  // rm
                     << axMaj << ' ' << axMin << ' ' << axPA << ' ' << refFreq
                     << '\n';
    }
  }
  cluster_stream << "## cID chunk_size source1 source2 ...\n";
  size_t clusterId = 0;
  bool isFirst = true;
  // We need to traverse in original cluster order
  std::set<std::string> clustersAdded;
  for (Model::const_iterator s = model.begin(); s != model.end(); ++s) {
    std::string cName = s->ClusterName();
    if (clustersAdded.count(cName) == 0) {
      clustersAdded.insert(cName);
      if (cName.empty())
        cluster_stream << clusters.size() + 1;
      else
        cluster_stream << clusterId;
      if (isFirst) {
        cluster_stream << ' ' << first_cluster_size;
        isFirst = false;
      } else
        cluster_stream << " 1";
      const std::vector<const ModelSource*>& sources = clusters[cName];
      for (std::vector<const ModelSource*>::const_iterator s = sources.begin();
           s != sources.end(); ++s) {
        for (size_t compIndex = 0; compIndex != (*s)->ComponentCount();
             ++compIndex) {
          const ModelComponent& c = (*s)->Component(compIndex);
          cluster_stream << ' ' << getSagecalSourceName(**s, c, compIndex);
        }
      }
      cluster_stream << '\n';
      ++clusterId;
    }
  }
}

Model Read(std::istream& sources_stream, std::istream& cluster_stream) {
  // now read the sky model
  // ### Name  | RA (hr,min,sec) | DEC (deg,min,sec) | I | Q | U | V | SI | RM |
  // eX (rad) | eY (rad) | eP (rad) | ref_freq */ NAME first letter : G/g
  // Gaussian
  //  D/d : disk
  //  R/r : ring
  //  S/s : shapelet
  //  else: point
  Model model;
  bool is_eof = SkipLines(sources_stream);
  while (!is_eof) {
    std::string source_name;
    sources_stream >> source_name;

    double iquv[4];
    double si_terms[3];
    double rahr, ramin, rasec, decd, decmin, decsec, dummy_RM, eX, eY, eP, f0;
    sources_stream >> rahr >> ramin >> rasec;
    sources_stream >> decd >> decmin >> decsec;
    sources_stream >> iquv[0] >> iquv[1] >> iquv[2] >> iquv[3];
    sources_stream >> si_terms[0] >> si_terms[1] >> si_terms[2];
    sources_stream >> dummy_RM;
    sources_stream >> eX >> eY >> eP;
    sources_stream >> f0;

    if (sources_stream) {
      ModelComponent component;
      if (rahr < 0.0)
        component.SetPosRA(-(-rahr + ramin / 60.0 + rasec / 3600.0) * M_PI /
                           12.0);
      else
        component.SetPosRA((rahr + ramin / 60.0 + rasec / 3600.0) * M_PI /
                           12.0);
      if (decd < 0.0)
        component.SetPosDec(-(-decd + decmin / 60.0 + decsec / 3600.0) * M_PI /
                            180.0);
      else
        component.SetPosDec((decd + decmin / 60.0 + decsec / 3600.0) * M_PI /
                            180.0);

      PowerLawSED sed;
      si_terms[1] *= M_LN10;
      si_terms[2] *= M_LN10 * M_LN10;
      sed.SetData(f0, iquv, si_terms);
      component.SetSED(std::move(sed));

      /* determine source type */
      if (source_name.empty())
        throw std::runtime_error("Empty source in model");
      else if (source_name[0] == 'G' || source_name[0] == 'g') {
        component.SetType(ModelComponent::GaussianSource);
        component.SetMajorAxis(eX);
        component.SetMinorAxis(eY);
        component.SetPositionAngle(eP + 0.5L * M_PI);
      } else if (source_name[0] == 'D' || source_name[0] == 'd') {
        throw std::runtime_error(
            "Disk-type sources are not supported, source name = " +
            source_name);
      } else if (source_name[0] == 'R' || source_name[0] == 'r') {
        throw std::runtime_error(
            "Ring-type sources are not supported, source name = " +
            source_name);
      } else if (source_name[0] == 'S' || source_name[0] == 's') {
        throw std::runtime_error(
            "Shapelet-type sources are not supported, source name = " +
            source_name);
      } else {
        component.SetType(ModelComponent::PointSource);
      }

      ModelSource source;
      source.SetName(source_name);
      source.AddComponent(std::move(component));
      model.AddSource(std::move(source));
    }
    SkipRestOfLine(sources_stream);
    is_eof = SkipLines(sources_stream);
  }

  std::map<std::string, ModelSource*> sources;
  for (ModelSource& source : model) {
    sources.emplace(source.Name(), &source);
  }

  // read the cluster file and set the cluster name of each source
  is_eof = SkipLines(cluster_stream);
  std::vector<char> buffer;
  while (!is_eof) {
    /* first read cluster number */
    bool new_line = ReadNextString(buffer, cluster_stream);
    if (!new_line) {
      // new cluster found
      const std::string buffer_id = buffer.data();

      // Read (and skip) no of chunks
      new_line = ReadNextString(buffer, cluster_stream);

      while (!new_line) {
        new_line = ReadNextString(buffer, cluster_stream);
        if (buffer.size() > 1) {
          // source found for this cluster
          const std::string source_name = buffer.data();
          std::map<std::string, ModelSource*>::iterator iter =
              sources.find(source_name);
          if (iter == sources.end())
            throw std::runtime_error("Invalid source name ('" + source_name +
                                     "') in cluster file");
          iter->second->SetClusterName(buffer_id);
        }
      }
    }
    is_eof = SkipLines(cluster_stream);
  }
  return model;
}

}  // namespace model::sagecal
