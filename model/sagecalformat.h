#ifndef SAGECAL_FORMAT_H_
#define SAGECAL_FORMAT_H_

#include "model.h"

namespace model::sagecal {

void Write(Model& model, std::ostream& sources_stream,
           std::ostream& cluster_stream, bool use_old_format,
           size_t first_cluster_size);

Model Read(std::istream& sources_stream, std::istream& cluster_stream);

}  // namespace model::sagecal

#endif
