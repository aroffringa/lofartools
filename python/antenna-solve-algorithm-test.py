#!/usr/bin/python3

import numpy
import numpy.linalg

def impute_diagonal_svd(data):
  sm = numpy.zeros(shape=data.shape)
  scratch = numpy.copy(data)
  for i in range(0, 50):
    [u,s,vh] = numpy.linalg.svd(scratch)
    sm[0,0] = s[0]
    newdiag = numpy.matmul(numpy.matmul(u, sm), vh)
    numpy.fill_diagonal(scratch, newdiag.diagonal())
  return scratch

def impute_diagonal_eig(data):
  sm = numpy.zeros(shape=data.shape)
  scratch = numpy.copy(data)
  for i in range(0, 50):
    [e,v] = numpy.linalg.eig(scratch)
    max_index = numpy.argmax(e)
    e0 = e[max_index]
    v0 = v[:,max_index]
    a = numpy.outer(e0*v0, numpy.conjugate(v0))
    numpy.fill_diagonal(scratch, a.diagonal())
  return scratch

def impute_data(data):
  matrix = numpy.zeros(shape=[data.shape[0], data.shape[1]])
  return impute_diagonal_eig(numpy.average(data, axis=2))

def ant_solve(data):
  n = data.shape[0]
  gains = numpy.zeros(shape=n) + 1
  new_gains = numpy.copy(gains)

  for repeat in range(0,100):
    for index, _ in enumerate(gains):
      # data_ij = g_i [model_ij] g_j*
      # g_i = sum_j ([data] / [model_ij] g_j) / n
      # (model_ij is just unitary)
      numerator = 0.0
      denominator = 0.0
      for j, _ in enumerate(gains):
        if j != index:
          numerator += data[index][j]
          denominator += gains[j]
      new_gains[index] = numerator / denominator
      
    gains = new_gains*0.2 + gains*0.8
  return gains

def ant_solve_data(data):
  n = data.shape[0]
  gains = numpy.zeros(shape=n) + 1
  new_gains = numpy.copy(gains)

  for repeat in range(0,100):
    for index, _ in enumerate(gains):
      numerator = 0.0
      denominator = 0.0
      for t in range(0, data.shape[2]):
        # data_ij = g_i [model_ij] g_j*
        # g_i = sum_j ([data] / [model_ij] g_j) / n
        # (model_ij is just unitary)
        for j, _ in enumerate(gains):
          if j != index:
            numerator += data[index,j,t] * gains[j]
            denominator += gains[j] * gains[j]
      new_gains[index] = numerator / denominator
      
    gains = new_gains*0.2 + gains*0.8
  return gains

def error(expected, result):
  return numpy.sqrt(numpy.sum((expected-result)**2)/expected.shape[0])
       
def data_distance(data, result):
  total = 0.0
  for i in range(0, data.shape[2]):
    diff = numpy.outer(result, result.conj()) - data[:,:,i]
    total += numpy.sum(diff**2)
  return numpy.sqrt(total/(diff.shape[0]*diff.shape[1]*data.shape[2]))
 
n_stations = 60
n_timesteps = 10
x = numpy.abs(numpy.random.normal(size=n_stations))+1

#noise = 0
#for i in range(0,1):
noise = 10000000

x_axis = []
svd_results = []
ant_solve_results = []

while(noise > 1e-1):
  data = numpy.zeros(shape=[n_stations,n_stations,n_timesteps])
  for i in range(0, n_timesteps):
    data[:,:,i] = numpy.outer(x, x.conj())
    data[:,:,i] += noise * numpy.random.normal(size=[n_stations,n_stations])
    data[:,:,i] = data[:,:,i] - numpy.diag(data[:,:,i].diagonal())
    
  original_data = numpy.copy(data)
  # Remove the diagonal

  result = impute_data(data)

  svd_error = 0.0
  svd_distance = 0.0
  [u,s,vh] = numpy.linalg.svd(result)
  gains = numpy.abs(u[:,0]*numpy.sqrt(s[0]))
  svd_error += error(x, gains)
  svd_distance += data_distance(original_data, gains)
    
  gains = ant_solve_data(data)
  ant_solve_error = error(x, gains)
  ant_solve_distance = data_distance(original_data, gains)
    
  print(f"{noise}, {svd_error}, {ant_solve_error}, {svd_distance}, {ant_solve_distance}")
  
  x_axis.append(1.0/noise)
  svd_results.append(svd_error)
  ant_solve_results.append(ant_solve_error)
    
  noise *= 0.9

import matplotlib.pyplot as pyplot
pyplot.plot(x_axis, svd_results, label='svd')
pyplot.plot(x_axis, ant_solve_results, label='ant-solve')
pyplot.xscale('log')
pyplot.xlabel('S/N ratio')
pyplot.yscale('log')
pyplot.ylabel('RMS error')
pyplot.legend()
pyplot.show()

pyplot.plot(x_axis, numpy.divide(ant_solve_results, svd_results))
pyplot.xscale('log')
pyplot.yscale('log')
pyplot.ylim(0.5, 2)
pyplot.show()

