#!/usr/bin/python3

import numpy
import solver_sim
import shutil
import tables
import math
import os

configuration = solver_sim.Configuration()
configuration.directions_solve="[3C196,3c197.1,4C+46.17,4C+47.27]"
configuration.directions_solve_indices=[0,1,2,3]

def simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev):
  shutil.copyfile(configuration.h5template, configuration.h5simulation)
  with tables.open_file(configuration.h5simulation, mode='r+') as h5file:
    # time,freq,antenna,dir,pol
    amplitudes = h5file.root.sol000.amplitude000.val[:]
    n_directions = amplitudes.shape[3]
    shape = numpy.delete(amplitudes.shape, 3)
    amplitudes = numpy.random.uniform(low=1.0, high=1.0 + amp_stddev, size=shape)*1+1
    for direction in range(0, n_directions):
      h5file.root.sol000.amplitude000.val[:,:,:,direction,:] = amplitudes
    
    phases = h5file.root.sol000.phase000.val[:]
    phases = phase_stddev*2*math.pi*numpy.random.normal(size=shape)
    for direction in range(0, n_directions):
      h5file.root.sol000.phase000.val[:,:,:,direction,:] = phases
  
  os.system(f'''DP3 \
  msin={configuration.ms} \
  msout=. \
  steps=[h5parmpredict] \
  h5parmpredict.sourcedb={configuration.model} \
  h5parmpredict.directions={configuration.directions_predict} \
  h5parmpredict.usebeammodel=False \
  h5parmpredict.applycal.parmdb={configuration.h5simulation} \
  h5parmpredict.applycal.correction=phase000 \
  h5parmpredict.applycal.steps=[amplitude,phase] \
  h5parmpredict.applycal.amplitude.correction=amplitude000 \
  h5parmpredict.applycal.phase.correction=phase000 > /dev/null
  ''')
  os.system(f'~/projects/lofartools/build/addnoise {vis_stddev} {configuration.ms}')
  
x_axis = []
as_sol_list = []
as_vis_list = []
ds_sol_list = []
ds_vis_list = []
di_sol_list = []
di_vis_list = []
hy_sol_list = []
hy_vis_list = []
lb_sol_list = []
lb_vis_list = []

factor = 0.0
for i in range(0, 22):
  amp_stddev = 0.2
  phase_stddev = 0.5
  vis_stddev = 0.025 * factor
  x_axis.append(vis_stddev)
  
  simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev)
  
  solver_sim.solve(configuration, "antennasolve")
  [as_sol_rms, as_vis_rms] = solver_sim.compare(configuration)
  as_sol_list.append(as_sol_rms)
  as_vis_list.append(as_vis_rms)
  
  solver_sim.solve(configuration, "directionsolve")
  [ds_sol_rms, ds_vis_rms] = solver_sim.compare(configuration)
  ds_sol_list.append(ds_sol_rms)
  ds_vis_list.append(ds_vis_rms)
  
  solver_sim.solve(configuration, "directioniterative")
  [di_sol_rms, di_vis_rms] = solver_sim.compare(configuration)
  di_sol_list.append(di_sol_rms)
  di_vis_list.append(di_vis_rms)
 
  solver_sim.solve(configuration, "hybrid")
  [hy_sol_rms, hy_vis_rms] = solver_sim.compare(configuration)
  hy_sol_list.append(hy_sol_rms)
  hy_vis_list.append(hy_vis_rms)
 
  solver_sim.solve(configuration, "lbfgs")
  [lb_sol_rms, lb_vis_rms] = solver_sim.compare(configuration)
  lb_sol_list.append(lb_sol_rms)
  lb_vis_list.append(lb_vis_rms)
 
  print(factor, as_sol_rms, ds_sol_rms, di_sol_rms, lb_sol_rms, as_vis_rms, ds_vis_rms, di_vis_rms, lb_vis_rms)

  if factor == 0.0:
    factor = 1.0
  else:
    factor = factor * 2.0

import matplotlib.pyplot as pyplot
pyplot.plot(x_axis, as_vis_list, label='Low-rank approximation')
pyplot.plot(x_axis, ds_vis_list, label='Direction solving')
pyplot.plot(x_axis, di_vis_list, label='Iterative')
pyplot.plot(x_axis, lb_vis_list, label='LBFGS')
pyplot.xscale('log')
pyplot.xlabel('Noise level')
pyplot.yscale('log')
pyplot.ylabel('Chi^2 value')
pyplot.ylim(bottom=1, top=1000)
pyplot.legend()
pyplot.show()

pyplot.plot(x_axis, as_sol_list, label='Low-rank approximation')
pyplot.plot(x_axis, ds_sol_list, label='Direction solving')
pyplot.plot(x_axis, di_sol_list, label='Iterative')
pyplot.plot(x_axis, lb_sol_list, label='LBFGS')
pyplot.xscale('log')
pyplot.xlabel('Noise level')
pyplot.yscale('log')
pyplot.ylabel('RMS solution error')
pyplot.ylim(top=100)
pyplot.legend()
pyplot.show()

