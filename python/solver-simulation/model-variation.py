#!/usr/bin/python3

import solver_sim
import numpy

configuration = solver_sim.Configuration()
configuration.directions_solve="[3C196,3c197.1,4C+46.17,4C+47.27]"
configuration.directions_solve_indices=[0,1,2,3]
configuration.as_stepsize=1

tester = solver_sim.SolverTester()

for i in range(0, 11):
  amp_stddev = 0.2
  phase_stddev = 0.2
  vis_stddev = 0.0025 * 4
  
  fluxes = numpy.array(configuration.final_fluxes)*0.1*i + (1.0 - 0.1*i)
  variation = 10.0*i # factor between lowest and highest flux
  print(fluxes)
  
  solver_sim.write_model(configuration, fluxes)
  solver_sim.simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev)
  tester.test_all(configuration, variation)

import matplotlib.pyplot as pyplot
tester.plot_all_chi_square()
pyplot.xlabel('Model variation (strongest/weakest)')
pyplot.show()

tester.plot_all_solution_rms()
pyplot.xlabel('Model variation (strongest/weakest)')
pyplot.show()


