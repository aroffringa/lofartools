#!/usr/bin/python3

import solver_sim
import numpy

configuration = solver_sim.Configuration()
configuration.directions_predict=""
#configuration.directions_solve="[3C196,3c197.1,4C+46.17,4C+47.27]"
#configuration.directions_solve_indices=[0,1,2,3]
configuration.directions_solve=""
configuration.directions_solve_indices=[0,1,2,3,4,5,6,7]
configuration.solutions_offset=1
configuration.as_stepsize=1
configuration.niter=10

x_axis = []
as_sol_list = []
as_vis_list = []
tester = solver_sim.SolverTester()

factor = 0.0

amp_stddev = 0.2
phase_stddev = 0.5
vis_stddev = 0.025 * 4
fluxes = [1, 2, 3, 4, 5, 6, 7, 8]
solver_sim.simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev, fluxes)
  
for i in range(1, 21):
  if i<2:
    configuration.as_lra_iter = i
  else:
    configuration.as_lra_iter = (i-1)*2
  
  tester.test_as(configuration, configuration.as_lra_iter)
 
import matplotlib.pyplot as pyplot
pyplot.plot(tester.x_axis, tester.as_vis_list, label='Low-rank approximation')
pyplot.xlabel('LRA iterations')
pyplot.yscale('log')
pyplot.ylabel('Chi^2 value')
pyplot.legend()
pyplot.show()

pyplot.plot(tester.x_axis, tester.as_sol_list, label='Low-rank approximation')
pyplot.xlabel('LRA iterations')
pyplot.yscale('log')
pyplot.ylabel('RMS solution error')
pyplot.legend()
pyplot.show()

