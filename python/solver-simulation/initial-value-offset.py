#!/usr/bin/python3

import solver_sim
import numpy

configuration = solver_sim.Configuration()
configuration.directions_solve="[3C196,3c197.1,4C+46.17,4C+47.27]"
configuration.directions_solve_indices=[0,1,2,3]
#configuration.directions_solve="[3c197.1]"
#configuration.directions_solve_indices=[1]
#configuration.directions_predict="[[3c197.1]]"

x_axis = []
as_sol_list = []
as_vis_list = []
ds_sol_list = []
ds_vis_list = []
di_sol_list = []
di_vis_list = []
hy_sol_list = []
hy_vis_list = []
lb_sol_list = []
lb_vis_list = []

factor = 0.0
for i in range(0, 11):
  amp_stddev = 0.2
  phase_stddev = 0.2
  vis_stddev = 0.0025 * 4
  initial_value_distance = i*0.5
  
  fluxes = [1,1,1,1,1,1,1,1]
  x_axis.append(initial_value_distance) # factor between lowest and highest flux
 
  solver_sim.simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev, fluxes, initial_value_distance)
  
  solver_sim.solve(configuration, "antennasolve")
  [as_sol_rms, as_vis_rms] = solver_sim.compare(configuration)
  as_sol_list.append(as_sol_rms)
  as_vis_list.append(as_vis_rms)
  
  solver_sim.solve(configuration, "directionsolve")
  [ds_sol_rms, ds_vis_rms] = solver_sim.compare(configuration)
  ds_sol_list.append(ds_sol_rms)
  ds_vis_list.append(ds_vis_rms)
  
  solver_sim.solve(configuration, "directioniterative")
  [di_sol_rms, di_vis_rms] = solver_sim.compare(configuration)
  di_sol_list.append(di_sol_rms)
  di_vis_list.append(di_vis_rms)
 
  solver_sim.solve(configuration, "hybrid")
  [hy_sol_rms, hy_vis_rms] = solver_sim.compare(configuration)
  hy_sol_list.append(hy_sol_rms)
  hy_vis_list.append(hy_vis_rms)
 
  solver_sim.solve(configuration, "lbfgs")
  [lb_sol_rms, lb_vis_rms] = solver_sim.compare(configuration)
  lb_sol_list.append(lb_sol_rms)
  lb_vis_list.append(lb_vis_rms)
 
  print(initial_value_distance)
  print(as_sol_rms, ds_sol_rms, di_sol_rms, hy_sol_rms, lb_sol_rms)
  print(as_vis_rms, ds_vis_rms, di_vis_rms, hy_vis_rms, lb_vis_rms)

  if factor == 0.0:
    factor = 1.0
  else:
    factor = factor * 2.0

xaxis_title = "Initial value offset"

import matplotlib.pyplot as pyplot
pyplot.plot(x_axis, as_sol_list, label='Low-rank approximation')
pyplot.plot(x_axis, ds_sol_list, label='Direction solving')
pyplot.plot(x_axis, di_sol_list, label='Iterative')
pyplot.plot(x_axis, hy_sol_list, label='Hybrid')
pyplot.plot(x_axis, lb_sol_list, label='LBFGS')
#pyplot.xscale('log')
pyplot.xlabel(xaxis_title)
pyplot.yscale('log')
pyplot.ylabel('RMS solution error')
#pyplot.ylim(top=100)
pyplot.legend()
pyplot.show()

pyplot.plot(x_axis, as_vis_list, label='Low-rank approximation')
pyplot.plot(x_axis, ds_vis_list, label='Direction solving')
pyplot.plot(x_axis, di_vis_list, label='Iterative')
pyplot.plot(x_axis, hy_vis_list, label='Hybrid')
pyplot.plot(x_axis, lb_vis_list, label='LBFGS')
#pyplot.xscale('log')
pyplot.xlabel(xaxis_title)
pyplot.yscale('log')
pyplot.ylabel('Chi^2 value')
#pyplot.ylim(bottom=0.01, top=1000)
pyplot.legend()
pyplot.show()

