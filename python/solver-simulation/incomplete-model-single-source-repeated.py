#!/usr/bin/python3

import solver_sim
import numpy

configuration = solver_sim.Configuration()
configuration.directions_predict="[[3C196],[3c197.1]]"
configuration.directions_solve="[3C196,3c197.1]"
configuration.directions_solve_indices=[1]
configuration.solutions_offset=1
configuration.as_stepsize=1

x_axis = []
as_sol_list = []
as_vis_list = []
ds_sol_list = []
ds_vis_list = []
di_sol_list = []
di_vis_list = []
hy_sol_list = []
hy_vis_list = []
lb_sol_list = []
lb_vis_list = []

factor = 0.0
for i in range(0, 25):
  amp_stddev = 0.2
  phase_stddev = 0.5
  vis_stddev = 0.025 * 4
  x_axis.append(i)
  
  fluxes = [1, 1, 1, 1, 1, 1, 1, 1]
  solver_sim.simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev, fluxes)
  
  solver_sim.solve(configuration, "antennasolve")
  [as_sol_rms, as_vis_rms] = solver_sim.compare(configuration)
  as_sol_list.append(as_sol_rms)
  as_vis_list.append(as_vis_rms)
  
  solver_sim.solve(configuration, "directionsolve")
  [ds_sol_rms, ds_vis_rms] = solver_sim.compare(configuration)
  ds_sol_list.append(ds_sol_rms)
  ds_vis_list.append(ds_vis_rms)
  
  solver_sim.solve(configuration, "directioniterative")
  [di_sol_rms, di_vis_rms] = solver_sim.compare(configuration)
  di_sol_list.append(di_sol_rms)
  di_vis_list.append(di_vis_rms)
 
  solver_sim.solve(configuration, "hybrid")
  [hy_sol_rms, hy_vis_rms] = solver_sim.compare(configuration)
  hy_sol_list.append(hy_sol_rms)
  hy_vis_list.append(hy_vis_rms)
 
  solver_sim.solve(configuration, "lbfgs")
  [lb_sol_rms, lb_vis_rms] = solver_sim.compare(configuration)
  lb_sol_list.append(lb_sol_rms)
  lb_vis_list.append(lb_vis_rms)
 
  print(as_sol_rms, ds_sol_rms, di_sol_rms, hy_sol_rms, lb_sol_rms)
  print(as_vis_rms, ds_vis_rms, di_vis_rms, hy_vis_rms, lb_vis_rms)

print(numpy.average(as_vis_list), numpy.average(ds_vis_list), numpy.average(di_vis_list), numpy.average(hy_vis_list), numpy.average(lb_vis_list))
print(f"Uncertainty of difference between antenna solve and direction solve: {numpy.std(numpy.array(as_vis_list) - numpy.array(ds_vis_list))}")
print(f"Uncertainty of difference between LBFGS solve and direction solve: {numpy.std(numpy.array(lb_vis_list) - numpy.array(ds_vis_list))}")
print(f"Uncertainty of difference between antenna solve and LBFGS solve: {numpy.std(numpy.array(as_vis_list) - numpy.array(lb_vis_list))}")

print(numpy.average(as_sol_list), numpy.average(ds_sol_list), numpy.average(di_sol_list), numpy.average(hy_sol_list), numpy.average(lb_sol_list))
print(f"Uncertainty of difference between antenna solve and direction solve: {numpy.std(numpy.array(as_sol_list) - numpy.array(ds_sol_list))}")
print(f"Uncertainty of difference between LBFGS solve and direction solve: {numpy.std(numpy.array(lb_sol_list) - numpy.array(ds_sol_list))}")
print(f"Uncertainty of difference between antenna solve and LBFGS solve: {numpy.std(numpy.array(as_sol_list) - numpy.array(lb_sol_list))}")

