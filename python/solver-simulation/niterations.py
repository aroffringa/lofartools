#!/usr/bin/python3

import solver_sim
import numpy

configuration = solver_sim.Configuration()
configuration.directions_predict=""
#configuration.directions_solve="[3C196,3c197.1,4C+46.17,4C+47.27]"
#configuration.directions_solve_indices=[0,1,2,3]
configuration.directions_solve=""
configuration.directions_solve_indices=[0,1,2,3,4,5,6,7]
configuration.solutions_offset=1
configuration.as_stepsize=1

tester = solver_sim.SolverTester()

amp_stddev = 0.2
phase_stddev = 0.5
vis_stddev = 0.025 * 4
#fluxes = [1, 2, 3, 4, 5, 6, 7, 8]
fluxes = [1, 1, 1, 1, 1, 1, 1, 1]
solver_sim.write_model(configuration, fluxes)
solver_sim.simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev)

for i in range(1, 24):
  if i<10:
    configuration.niter = i
  elif i<20:
    configuration.niter = (i-9)*10
  else:
    configuration.niter = (i-10)*20
    
  tester.test_all(configuration, configuration.niter)
  
import matplotlib.pyplot as pyplot
tester.plot_all_chi_square()
pyplot.xscale('log')
pyplot.xlabel('Iterations')
pyplot.yscale('log')
pyplot.ylim(top=8)
pyplot.show()

tester.plot_all_solution_rms()
pyplot.xscale('log')
pyplot.xlabel('Iterations')
pyplot.yscale('log')
pyplot.ylabel('RMS solution error')
pyplot.ylim(top=4)
pyplot.legend()
pyplot.show()

