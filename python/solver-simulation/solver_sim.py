#!/usr/bin/python3

import os
import math
import matplotlib.pyplot as pyplot
import numpy
import shutil
import subprocess
import tables

class Configuration:
  def __init__(self):
    self.basedir="/home/anoko/Data/New-Solver-2023-06-29"
    self.ms=f"{self.basedir}/L258627-60s.ms"
    self.h5template=f"{self.basedir}/template.h5"
    self.h5simulation=f"{self.basedir}/simulated-solutions.h5"
    self.h5solutions=f"{self.basedir}/solutions.h5"
    self.model=f"{self.basedir}/dp3-simulated-model.txt"
    #configuration.directions_predict="[[3C196],[3c197.1]]"
    self.directions_predict=""
    self.directions_compare_predict=""
    #self.directions_solve="[3C196,3c197.1,4C+46.17,4C+47.27]"
    self.directions_solve=""
    #self.directions_solve_indices=[0,1,2,3]
    self.directions_solve_indices=[]
    self.final_fluxes=[100,12,5,3,2,1,1,1]
    self.solutions_offset=0
    self.as_stepsize=1
    self.as_lra_iter=6
    self.as_power_iter=2
    self.iterations_per_algorithm={
      'antennasolve': 10,
      'directionsolve': 25,
      'directioniterative': 150,
      'hybrid': 100,
      'lbfgs': 20
    }
    
  def set_iterations(self, n_iterations):
    self.iterations_per_algorithm={
      'antennasolve': n_iterations,
      'directionsolve': n_iterations,
      'directioniterative': n_iterations,
      'hybrid': n_iterations,
      'lbfgs': n_iterations
    }
    
class SolverTester:
  def __init__(self):
    self.x_axis = []
    self.as_sol_list = []
    self.as_vis_list = []
    self.ds_sol_list = []
    self.ds_vis_list = []
    self.di_sol_list = []
    self.di_vis_list = []
    self.hy_sol_list = []
    self.hy_vis_list = []
    self.lb_sol_list = []
    self.lb_vis_list = []

  def test_as(self, configuration, x_value):
    self.x_axis.append(x_value)
    
    solve(configuration, "antennasolve")
    [as_sol_rms, as_vis_rms] = compare(configuration)
    self.as_sol_list.append(as_sol_rms)
    self.as_vis_list.append(as_vis_rms)
    print(x_value, as_sol_rms, as_vis_rms)
  
  def test_all(self, configuration, x_value):
    self.x_axis.append(x_value)
    
    solve(configuration, "antennasolve")
    [as_sol_rms, as_vis_rms] = compare(configuration)
    self.as_sol_list.append(as_sol_rms)
    self.as_vis_list.append(as_vis_rms)
    
    solve(configuration, "directionsolve")
    [ds_sol_rms, ds_vis_rms] = compare(configuration)
    self.ds_sol_list.append(ds_sol_rms)
    self.ds_vis_list.append(ds_vis_rms)
    
    solve(configuration, "directioniterative")
    [di_sol_rms, di_vis_rms] = compare(configuration)
    self.di_sol_list.append(di_sol_rms)
    self.di_vis_list.append(di_vis_rms)
   
    solve(configuration, "hybrid")
    [hy_sol_rms, hy_vis_rms] = compare(configuration)
    self.hy_sol_list.append(hy_sol_rms)
    self.hy_vis_list.append(hy_vis_rms)
   
    solve(configuration, "lbfgs")
    [lb_sol_rms, lb_vis_rms] = compare(configuration)
    self.lb_sol_list.append(lb_sol_rms)
    self.lb_vis_list.append(lb_vis_rms)
   
    print(as_vis_rms, ds_vis_rms, di_vis_rms, hy_vis_rms, lb_vis_rms)
    print(as_sol_rms, ds_sol_rms, di_sol_rms, hy_sol_rms, lb_sol_rms)

  def plot_all_chi_square(self):
    pyplot.plot(self.x_axis, self.as_vis_list, label='Low-rank approximation')
    pyplot.plot(self.x_axis, self.ds_vis_list, label='Direction solving')
    pyplot.plot(self.x_axis, self.di_vis_list, label='Iterative')
    pyplot.plot(self.x_axis, self.hy_vis_list, label='Hybrid')
    pyplot.plot(self.x_axis, self.lb_vis_list, label='LBFGS')
    pyplot.yscale('log')
    pyplot.ylabel('Chi² value')
    pyplot.legend()

  def plot_all_solution_rms(self):
    pyplot.plot(self.x_axis, self.as_sol_list, label='Low-rank approximation')
    pyplot.plot(self.x_axis, self.ds_sol_list, label='Direction solving')
    pyplot.plot(self.x_axis, self.di_sol_list, label='Iterative')
    pyplot.plot(self.x_axis, self.hy_sol_list, label='Hybrid')
    pyplot.plot(self.x_axis, self.lb_sol_list, label='LBFGS')
    pyplot.yscale('log')
    pyplot.ylabel('RMS solution amplitude error')
    pyplot.legend()

def snr_test(configuration):
  tester = SolverTester()

  factor = 0.0
  for i in range(0, 22):
    amp_stddev = 0.2
    phase_stddev = 0.5
    if i==0:
      vis_stddev = 0
    else:
      vis_stddev = 0.02 * math.pow(2, i-1)
    
    fluxes = [1, 1, 1, 1, 1, 1, 1, 1]
    write_model(configuration, fluxes)
    simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev)
    
    tester.test_all(configuration, vis_stddev)

  tester.plot_all_chi_square()
  pyplot.xscale('log')
  pyplot.xlabel('Noise level')
  pyplot.show()

  tester.plot_all_solution_rms()
  pyplot.xscale('log')
  pyplot.xlabel('Noise level')
  pyplot.ylim(top=100)
  pyplot.show()

def simulate_model(configuration, n_directions):
  os.system(f"editmodel -skymodel temp.txt -simpopulation 08h13m36.068s 48d13m02.581s 4 1 10 > /dev/null")
  os.system(f"~/projects/lofartools/build/cluster temp.txt temp2.txt {n_directions}")
  os.system(f"editmodel -skymodel {configuration.model} temp2.txt")
  os.remove('temp.txt')
  os.remove('temp2.txt')
  
def write_model(configuration, fluxes):
  modelstr=f"""Format = Name, Patch, Type, Ra, Dec, I, Q, U, V, SpectralIndex, LogarithmicSI, ReferenceFrequency='150.e6', MajorAxis, MinorAxis, Orientation

, 3C196, POINT, , , , , , , , , , , ,
3C196, 3C196, POINT, 08:13:35.925, 48.13.00.061, {fluxes[0]}, 0, 0, 0, [0], true, 150000000, , ,
, 3c197.1, POINT, , , , , , , , , , , ,
3c197.1, 3c197.1, POINT, 08:21:33.582, 47.02.30.457, {fluxes[1]}, 0, 0, 0, [0], true, 150000000, , ,
, 4C+46.17, POINT, , , , , , , , , , , ,
4C+46.17, 4C+46.17, POINT, 08:14:30.309, 45.56.39.57, {fluxes[2]}, 0, 0, 0, [0], true, 150000000, , ,
, 4C+47.27, POINT, , , , , , , , , , , ,
4C+47.27, 4C+47.27, POINT, 08:04:13.91, 47.04.43.07, {fluxes[3]}, 0, 0, 0, [0], true, 150000000, , ,
, 4C+49.17, POINT, , , , , , , , , , , ,
4C+49.17, 4C+49.17, POINT, 08:10:55.936, 49.31.35.93, {fluxes[4]}, 0, 0, 0, [0], true, 150000000, , ,
, J080135.35+500943.9, POINT, , , , , , , , , , , ,
J080135.35+500943.9, J080135.35+500943.9, POINT, 08:01:35.112, 50.09.38.696, {fluxes[5]}, 0, 0, 0, [0], true, 150000000, , ,
, J080508+480151, POINT, , , , , , , , , , , ,
J080508+480151, J080508+480151, POINT, 08:05:08.323, 48.01.51.74, {fluxes[6]}, 0, 0, 0, [0], true, 150000000, , ,
, J080645+484137, POINT, , , , , , , , , , , ,
J080645+484137, J080645+484137, POINT, 08:06:46.536, 48.41.19.654, {fluxes[7]}, 0, 0, 0, [0], true, 150000000, , ,
"""
  with open(configuration.model, 'w') as f:
    f.write(modelstr)
  
def simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev):
  # Perform a quick solve to make a h5parm template
  os.system(f'''DP3 msin={configuration.ms} \
    msout= \
    steps=[ddecal] \
    ddecal.sourcedb={configuration.model} \
    ddecal.h5parm={configuration.h5simulation} \
    ddecal.solint=5 \
    ddecal.maxiter=1 > /dev/null''')
  with tables.open_file(configuration.h5simulation, mode='r+') as h5file:
    # time,freq,antenna,dir,pol
    amplitudes = h5file.root.sol000.amplitude000.val[:]
    n_directions = amplitudes.shape[3]
    shape = amplitudes.shape
    amplitudes = numpy.random.uniform(low=max(0.5, 1.0 - amp_stddev), high=1.0 + amp_stddev, size=shape) + configuration.solutions_offset
    for direction in range(0, n_directions):
      h5file.root.sol000.amplitude000.val[:] = amplitudes
    
    phases = h5file.root.sol000.phase000.val[:]
    phases = phase_stddev*2*math.pi*numpy.random.normal(size=shape)
    for direction in range(0, n_directions):
      h5file.root.sol000.phase000.val[:] = phases
  
  os.system(f'''DP3 \
  msin={configuration.ms} \
  msout=. \
  steps=[h5parmpredict] \
  h5parmpredict.sourcedb={configuration.model} \
  h5parmpredict.directions={configuration.directions_predict} \
  h5parmpredict.usebeammodel=False \
  h5parmpredict.applycal.parmdb={configuration.h5simulation} \
  h5parmpredict.applycal.correction=phase000 \
  h5parmpredict.applycal.steps=[amplitude,phase] \
  h5parmpredict.applycal.amplitude.correction=amplitude000 \
  h5parmpredict.applycal.phase.correction=phase000 > /dev/null''')
  os.system(f'~/projects/lofartools/build/addnoise {vis_stddev} {configuration.ms}')
  
def solve(configuration, solver_type):
  if solver_type=="antennasolve":
    stepsize=configuration.as_stepsize
    lra=f'''    ddecal.lra.iterations={configuration.as_lra_iter} \
    ddecal.lra.power_iterations={configuration.as_power_iter} \
    ddecal.stepsize={stepsize} '''
  else:
    lra=''
  alg_iters=configuration.iterations_per_algorithm[solver_type]
  os.system(f'''DP3 msin={configuration.ms} \
    msout= \
    steps=[ddecal] \
    ddecal.sourcedb={configuration.model} \
    ddecal.directions={configuration.directions_solve} \
    ddecal.h5parm={configuration.h5solutions} \
    ddecal.solint=5 \
    ddecal.maxiter={alg_iters} \
    ddecal.tolerance=1e-7 \
    {lra} \
    ddecal.usebeammodel=False \
    ddecal.solveralgorithm={solver_type} |grep "estimating gains" ''')
  
def compare(configuration):
  with tables.open_file(configuration.h5simulation) as h5_reference:
    if configuration.directions_solve_indices:
      reference_amplitudes = h5_reference.root.sol000.amplitude000.val[:,:,:,configuration.directions_solve_indices,:]
      reference_phases = h5_reference.root.sol000.phase000.val[:,:,:,configuration.directions_solve_indices,:]
    else:
      reference_amplitudes = h5_reference.root.sol000.amplitude000.val[:]
      reference_phases = h5_reference.root.sol000.phase000.val[:]
    # time,freq,antenna,dir,pol
    reference_phases = reference_phases - reference_phases[:,:,0,:,:]
  with tables.open_file(configuration.h5solutions) as h5_solution:
    solved_amplitudes = h5_solution.root.sol000.amplitude000.val[:]
    solved_phases = h5_solution.root.sol000.phase000.val[:]
    solved_phases = solved_phases - solved_phases[:,:,0,:,:]

  reference = reference_amplitudes #* numpy.exp(1j*reference_phases)
  solved = solved_amplitudes #* numpy.exp(1j*solved_phases)
  difference = reference - solved
  solution_rms = numpy.sqrt(numpy.sum(numpy.real(difference * numpy.conj(difference))) / difference.size)
  
  os.system(f'''DP3 \
  msin={configuration.ms} \
  msout=calibration-result.ms \
  msout.overwrite=True \
  steps=[h5parmpredict] \
  h5parmpredict.sourcedb={configuration.model} \
  h5parmpredict.directions={configuration.directions_compare_predict} \
  h5parmpredict.operation=subtract \
  h5parmpredict.applycal.parmdb={configuration.h5solutions} \
  h5parmpredict.applycal.correction=phase000 \
  h5parmpredict.applycal.steps=[amplitude,phase] \
  h5parmpredict.applycal.amplitude.correction=amplitude000 \
  h5parmpredict.applycal.phase.correction=phase000 > /dev/null
  ''')
  visibilityrms_output = subprocess.run(['/home/anoko/projects/lofartools/build/visibilityrms', 'calibration-result.ms'], capture_output=True)
  visibility_rms = float(visibilityrms_output.stdout)
  shutil.rmtree('calibration-result.ms')
  return [solution_rms, visibility_rms]

