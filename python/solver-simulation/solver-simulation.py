#!/usr/bin/python3

import os
import math
import numpy
import shutil
import subprocess
import tables

basedir="/home/anoko/Data/New-Solver-2023-06-29"
ms=f"{basedir}/L258627-60s.ms"
h5template=f"{basedir}/template.h5"
h5simulation=f"{basedir}/simulated-solutions.h5"
h5solutions=f"{basedir}/solutions.h5"
model=f"{basedir}/dp3-model.txt"
directions="[3C196]"
directions_predict="[[3C196]]"

def simulate_solutions(amp_stddev, phase_stddev, vis_stddev):
  shutil.copyfile(h5template, h5simulation)
  with tables.open_file(h5simulation, mode='r+') as h5file:
    amplitudes = h5file.root.sol000.amplitude000.val[:]
    amplitudes = numpy.random.uniform(low=1.0, high=1.0 + amp_stddev, size=amplitudes.shape)
    h5file.root.sol000.amplitude000.val[:] = amplitudes
    
    phases = h5file.root.sol000.phase000.val[:]
    phases = phase_stddev*2*math.pi*numpy.random.normal(size=phases.shape)
    h5file.root.sol000.phase000.val[:] = phases
  
  os.system(f'''DP3 \
  msin={ms} \
  msout=. \
  steps=[h5parmpredict] \
  h5parmpredict.sourcedb={model} \
  h5parmpredict.directions={directions_predict} \
  h5parmpredict.applycal.parmdb={h5simulation} \
  h5parmpredict.applycal.correction=phase000 \
  h5parmpredict.applycal.steps=[amplitude,phase] \
  h5parmpredict.applycal.amplitude.correction=amplitude000 \
  h5parmpredict.applycal.phase.correction=phase000 > /dev/null
  ''')
  os.system(f'~/projects/lofartools/build/addnoise {vis_stddev} {ms}')
  
def solve(solver_type):
  os.system(f'''DP3 msin={ms} \
    msout= \
    steps=[ddecal] \
    ddecal.sourcedb={model} \
    ddecal.directions={directions} \
    ddecal.h5parm={h5solutions} \
    ddecal.solint=5 \
    ddecal.maxiter=100 \
    ddecal.tolerance=1e-7 \
    ddecal.stepsize=0.2 \
    ddecal.usebeammodel=False \
    ddecal.solveralgorithm={solver_type} > /dev/null ''')
  
def compare():
  with tables.open_file(h5simulation) as h5_reference:
    reference_amplitudes = h5_reference.root.sol000.amplitude000.val[:]
    reference_phases = h5_reference.root.sol000.phase000.val[:]
    # time,freq,antenna,dir,pol
    reference_phases = reference_phases - reference_phases[:,:,0,:,:]
  with tables.open_file(h5solutions) as h5_solution:
    solved_amplitudes = h5_solution.root.sol000.amplitude000.val[:]
    solved_phases = h5_solution.root.sol000.phase000.val[:]
    solved_phases = solved_phases - solved_phases[:,:,0,:,:]

  reference = reference_amplitudes * numpy.exp(1j*reference_phases)
  solved = solved_amplitudes * numpy.exp(1j*solved_phases)
  difference = reference - solved
  solution_rms = numpy.sqrt(numpy.sum(numpy.real(difference * numpy.conj(difference))) / difference.size)
  
  os.system(f'''DP3 \
  msin={ms} \
  msout=calibration-result.ms \
  msout.overwrite=True \
  steps=[h5parmpredict] \
  h5parmpredict.sourcedb={model} \
  h5parmpredict.directions={directions_predict} \
  h5parmpredict.operation=subtract \
  h5parmpredict.applycal.parmdb={h5solutions} \
  h5parmpredict.applycal.correction=phase000 \
  h5parmpredict.applycal.steps=[amplitude,phase] \
  h5parmpredict.applycal.amplitude.correction=amplitude000 \
  h5parmpredict.applycal.phase.correction=phase000 > /dev/null
  ''')
  visibilityrms_output = subprocess.run(['/home/anoko/projects/lofartools/build/visibilityrms', 'calibration-result.ms'], capture_output=True)
  visibility_rms = float(visibilityrms_output.stdout)
  shutil.rmtree('calibration-result.ms')
  return [solution_rms, visibility_rms]

factor = 0.0
for i in range(0, 25):
  amp_stddev = 0.2
  phase_stddev = 1
  vis_stddev = 0.0025 * factor
  simulate_solutions(amp_stddev, phase_stddev, vis_stddev)
  solve("antennasolve")
  [as_sol_rms, as_vis_rms] = compare()
  simulate_solutions(amp_stddev, phase_stddev, vis_stddev)
  solve("directionsolve")
  [ds_sol_rms, ds_vis_rms] = compare()
  simulate_solutions(amp_stddev, phase_stddev, vis_stddev)
  solve("directioniterative")
  [di_sol_rms, di_vis_rms] = compare()
 
  print(factor, as_sol_rms, ds_sol_rms, di_sol_rms, as_vis_rms, ds_vis_rms, di_vis_rms)

  if factor == 0.0:
    factor = 1.0
  else:
    factor = factor * 2.0

