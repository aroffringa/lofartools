#!/usr/bin/python3

import solver_sim
import numpy

configuration = solver_sim.Configuration()
configuration.directions_predict=""
configuration.directions_solve=""
configuration.directions_solve_indices=[]
configuration.solutions_offset=0
configuration.as_stepsize=1
configuration.as_lra_iter=25
configuration.as_power_iter=10
configuration.set_iterations(150)

tester = solver_sim.SolverTester()

amp_stddev = 0.2
phase_stddev = 0.2
vis_stddev = 0.01 #0.025 * 4
for i in range(1, 24):
  if i<10:
    n_sources = i
  elif i<20:
    n_sources = (i-9)*10
  else:
    n_sources = (i-10)*20
    
  solver_sim.simulate_model(configuration, n_sources)
  solver_sim.simulate_solutions(configuration, amp_stddev, phase_stddev, vis_stddev)

  tester.test_all(configuration, n_sources)
  
import matplotlib.pyplot as pyplot
tester.plot_all_chi_square()
pyplot.xscale('log')
pyplot.xlabel('Iterations')
pyplot.yscale('log')
pyplot.ylim(top=8)
pyplot.show()

tester.plot_all_solution_rms()
pyplot.xscale('log')
pyplot.xlabel('Iterations')
pyplot.yscale('log')
pyplot.ylabel('RMS solution error')
pyplot.ylim(top=4)
pyplot.legend()
pyplot.show()

