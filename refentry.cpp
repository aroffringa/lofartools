#include <iostream>

#include <aocommon/banddata.h>

#include "ref/reffile.h"

#include <boost/filesystem/operations.hpp>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

int main(int argc, char* argv[]) {
  if (argc < 4)
    std::cout << "Syntax: refentry <ms> <node> <sap>\n";
  else {
    std::string path = boost::filesystem::absolute(argv[1]).string();
    casacore::MeasurementSet ms(argv[1]);
    aocommon::BandData band(ms.spectralWindow());
    std::ostringstream freqStr;
    freqStr << band.CentreFrequency() * 1e-6;

    AOTools::RefFileEntry entry;
    entry.SetPath(path);
    entry.SetFrequency(freqStr.str());
    entry.SetSize(0);
    entry.SetNode(argv[2]);
    entry.SetSAP(atof(argv[3]));
    entry.Write(std::cout);
  }
}
