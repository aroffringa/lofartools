#ifndef MATH_GAUSSIAN_CONVERSIONS_H_
#define MATH_GAUSSIAN_CONVERSIONS_H_

#include <aocommon/matrix2x2.h>

#include <cmath>

namespace math {

inline void ToGaussianCovariance(double fwhm_maj, double fwhm_min,
                                 double position_angle, double& sx_sx,
                                 double& sx_sy, double& sy_sy) {
  const long double kSigmaToBeam = 2.0L * std::sqrt(2.0L * std::log(2.0L));
  fwhm_maj /= kSigmaToBeam;
  fwhm_min /= kSigmaToBeam;
  const double saSq = fwhm_maj * fwhm_maj;
  const double sbSq = fwhm_min * fwhm_min;
  const double cosAngle = std::cos(position_angle);
  const double sinAngle = std::sin(position_angle);
  const double cosAngleSq = cosAngle * cosAngle;
  const double sinAngleSq = sinAngle * sinAngle;
  // The covariance matrix is Rotation x Scaling x Rotation^T:
  // sxsx sxsy    cos a -sin a  maj^2 0    cos a  sin a
  // sxsy sysy = (sin a  cos a)  0 min^2 (-sin a  cos a)
  //
  //             maj^2 cos a  -min^2 sin a     cos a  sin a)
  //          =( maj^2 sin a   min^2 cos a ) (-sin a  cos a)
  sx_sx = saSq * cosAngleSq + sbSq * sinAngleSq;
  sx_sy = saSq * sinAngle * cosAngle - sbSq * cosAngle * sinAngle;
  sy_sy = saSq * sinAngleSq + sbSq * cosAngleSq;
}

inline void FromGaussianCovariance(double sx_sx, double sx_sy, double sy_sy,
                                   double& fwhm_maj, double& fwhm_min,
                                   double& position_angle) {
  const long double kSigmaToBeam = 2.0L * std::sqrt(2.0L * std::log(2.0L));
  double cov[4];
  cov[0] = sx_sx;
  cov[1] = sx_sy;
  cov[2] = sx_sy;
  cov[3] = sy_sy;

  double e1, e2, vec1[2], vec2[2];
  aocommon::Matrix2x2::EigenValuesAndVectors(cov, e1, e2, vec1, vec2);
  if (std::isfinite(e1)) {
    fwhm_maj = std::sqrt(std::fabs(e1)) * kSigmaToBeam;
    fwhm_min = std::sqrt(std::fabs(e2)) * kSigmaToBeam;
    if (fwhm_maj < fwhm_min) {
      std::swap(fwhm_maj, fwhm_min);
      vec1[0] = vec2[0];
      vec1[1] = vec2[1];
    }
    position_angle = atan2(vec1[1], vec1[0]);
  } else {
    fwhm_maj = std::sqrt(std::fabs(sx_sx)) * kSigmaToBeam;
    fwhm_min = std::sqrt(std::fabs(sx_sx)) * kSigmaToBeam;
    position_angle = 0.0;
  }
}

}  // namespace math

#endif
