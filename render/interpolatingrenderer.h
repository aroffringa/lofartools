#ifndef INTERPOLATED_RENDERER_H
#define INTERPOLATED_RENDERER_H

#include <cstring>

#include <aocommon/uvector.h>

class InterpolatingRenderer {
 public:
  InterpolatingRenderer(size_t kernel_size)
      : x_kernel_(kernel_size + (kernel_size + 1) % 2),
        y_kernel_(kernel_size + (kernel_size + 1) % 2) {}

  /**
   * This will render a source and sinc-interpolate it so it
   * can be on non-integer positions.
   */
  static void RenderSource(float* image, size_t width, size_t height,
                           float flux, double x, double y);

  /**
   * This will render a source and sinc-interpolate with a window it so it
   * can be on non-integer positions. A Hann-window is used.
   */
  void RenderWindowedSource(float* image, size_t width, size_t height,
                            float brightness, float x, float y);

 private:
  size_t kernel_size_;
  aocommon::UVector<float> x_kernel_;
  aocommon::UVector<float> y_kernel_;
};

#endif
