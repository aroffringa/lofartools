#include "interpolatingrenderer.h"

#include <aocommon/uvector.h>

#include <cmath>

namespace {
aocommon::UVector<float> MakeSincKernel(double value, size_t n) {
  aocommon::UVector<float> kernel(std::min<size_t>(n, 128) + 1);
  const int mid = kernel.size() / 2;
  const double fraction = value - std::floor(value);
  for (size_t i = 0; i != kernel.size(); ++i) {
    const double value = (int(i) - mid - fraction) * M_PI;
    kernel[i] = (value == 0) ? 1.0 : std::sin(value) / value;
  }
  return kernel;
}

void MakeWindowedKernel(double value, size_t n,
                        aocommon::UVector<float>& kernel) {
  const int midH = n / 2;
  const float fraction = value - std::floor(value);
  for (size_t i = 0; i != n; ++i) {
    const float xi = (int(i) - midH - fraction) * M_PI;
    const float hann = std::cos(xi / n);
    kernel[i] = (xi == 0.0) ? 1.0 : hann * hann * std::sin(xi) / xi;
  }
}

}  // namespace

void InterpolatingRenderer::RenderSource(float* image, size_t width,
                                         size_t height, float brightness,
                                         double x, double y) {
  const aocommon::UVector<float> x_sinc = MakeSincKernel(x, width);
  const aocommon::UVector<float> y_sinc = MakeSincKernel(y, height);

  const int mid_x = x_sinc.size() / 2;
  const int mid_y = y_sinc.size() / 2;
  const int x_offset = std::floor(x) - mid_x;
  const int y_offset = std::floor(y) - mid_y;
  const size_t start_x = std::max<int>(x_offset, 0);
  const size_t start_y = std::max<int>(y_offset, 0);
  const size_t end_x = std::min<size_t>(x_offset + x_sinc.size(), width);
  const size_t end_y = std::min<size_t>(y_offset + y_sinc.size(), height);

  for (size_t yi = start_y; yi != end_y; ++yi) {
    float* ptr = &image[yi * width];
    const double y_value = brightness * y_sinc[yi - y_offset];
    for (size_t xi = start_x; xi != end_x; ++xi) {
      ptr[xi] += y_value * x_sinc[xi - x_offset];
    }
  }
}

void InterpolatingRenderer::RenderWindowedSource(float* image, size_t width,
                                                 size_t height, float flux,
                                                 float x, float y) {
  const size_t n = x_kernel_.size();

  MakeWindowedKernel(x, n, x_kernel_);
  MakeWindowedKernel(y, n, y_kernel_);

  const int mid_x = n / 2;
  const int mid_y = n / 2;
  const int x_offset = std::floor(x) - mid_x;
  const int y_offset = std::floor(y) - mid_y;
  const size_t start_x = std::max<int>(x_offset, 0);
  const size_t start_y = std::max<int>(y_offset, 0);
  const size_t end_x =
      std::max<int>(std::min<int>(x_offset + int(n), int(width)), int(start_x));
  const size_t end_y = std::max<int>(
      std::min<int>(y_offset + int(n), int(height)), int(start_y));
  for (size_t yi = start_y; yi != end_y; ++yi) {
    float* ptr = &image[yi * width];
    const float vFlux = flux * y_kernel_[yi - y_offset];
    for (size_t xi = start_x; xi != end_x; ++xi) {
      ptr[xi] += vFlux * x_kernel_[xi - x_offset];
    }
  }
}
