#include <cmath>
#include <fstream>
#include <iostream>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/tables/Tables/ScalarColumn.h>

const long double speedOfLight = 299792458.0L;

int main(int argc, char* argv[]) {
  if (argc <= 1)
    std::cerr << "Syntax: sensitivity <ms>\n";
  else {
    casacore::MeasurementSet ms(argv[1]);
    casacore::Table antTable(ms.antenna());
    casacore::ScalarColumn<casacore::String> nameCol(
        antTable,
        casacore::MSAntenna::columnName(casacore::MSAntennaEnums::NAME));
    casacore::ArrayColumn<double> posCol(
        antTable,
        casacore::MSAntenna::columnName(casacore::MSAntennaEnums::POSITION));

    size_t n = 100;
    std::vector<double> densityNoRS(n), densityWithRS(n);
    double densityRange = 20000.0;  // 20 kLambda's
    double bandwidth = 48e6;        // 48 MHz bandwidth
    double freqStart = 120e6;       // 120 MHz
    double freqEnd = freqStart + bandwidth;

    struct Antenna {
      std::string name;
      double x, y, z;
    };
    std::vector<Antenna> antennas;

    for (size_t row = 0; row != antTable.nrow(); ++row) {
      Antenna antenna;
      antenna.name = nameCol(row);
      casacore::Array<double> pos = posCol.get(row);
      antenna.x = pos.data()[0];
      antenna.y = pos.data()[1];
      antenna.z = pos.data()[2];
      antennas.emplace_back(antenna);
    }

    for (size_t a1 = 0; a1 != antennas.size(); ++a1) {
      for (size_t a2 = 0; a2 != antennas.size(); ++a2) {
        bool a1IsRemote = antennas[a1].name.substr(0, 2) == "RS";
        bool a2IsRemote = antennas[a2].name.substr(0, 2) == "RS";
        bool a1IsIntern = !a1IsRemote && antennas[a1].name.substr(0, 2) != "CS";
        bool a2IsIntern = !a2IsRemote && antennas[a2].name.substr(0, 2) != "CS";
        if (a1 != a2) {
          double dx = antennas[a1].x - antennas[a2].x;
          double dy = antennas[a1].y - antennas[a2].y;
          double dz = antennas[a1].z - antennas[a2].z;
          double dist = std::sqrt(dx * dx + dy * dy + dz * dz);
          std::cout << antennas[a1].name << '\t' << antennas[a2].name << '\t'
                    << dist << '\t';
          std::cout << dist / speedOfLight * freqStart << '\t'
                    << dist / speedOfLight * freqEnd << '\n';
          double indexFactor = dist / speedOfLight * n / densityRange;
          size_t startIndex =
              std::min<size_t>(n, round(freqStart * indexFactor));
          size_t endIndex = std::min<size_t>(n, round(freqEnd * indexFactor));

          for (size_t index = startIndex; index != endIndex; ++index) {
            if (!a1IsIntern && !a2IsIntern) densityNoRS[index]++;
            double rsDensity =
                (a1IsRemote ? 2.0 : 1.0) * (a2IsRemote ? 2.0 : 1.0);
            densityWithRS[index] += rsDensity;
          }
        }
      }
    }
    double avgWavelength = speedOfLight / (freqStart + bandwidth * 0.5);
    std::cout << "Using average wavelength: " << avgWavelength << '\n';
    std::ofstream densityFile("density.txt");
    for (size_t i = 0; i != densityNoRS.size(); ++i) {
      double distLambda = densityRange * i / n;
      double density = densityNoRS[i];
      double angularSize =
          1.22 * avgWavelength /
          (distLambda * speedOfLight / (freqStart + bandwidth * 0.5));
      densityFile << distLambda << '\t' << angularSize * 180.0 * 60.0 / M_PI
                  << '\t' << density << '\t' << densityWithRS[i] << '\n';
    }
    for (double angularSize = 0.0; angularSize < 10.0; angularSize += 0.1) {
      double angularSizeRad = angularSize * M_PI / (180.0 * 60.0);
      double distLambda = ((1.22 * avgWavelength) / angularSizeRad) *
                          (freqStart + bandwidth * 0.5) / speedOfLight;
      std::cout << angularSize << '\t' << distLambda << '\n';
    }
  }
  return 0;
}
