#include <aocommon/fits/fitsreader.h>

#include "model/model.h"

#include <iostream>

using namespace aocommon;

void perform(const char* modelFileIn, const char* modelFileOut,
             const char** fitsFiles, size_t nFitsFiles) {
  Model model(modelFileIn);

  for (ModelSource& source : model) {
    for (ModelComponent& component : source) component.SetSED(MeasuredSED());
  }

  aocommon::UVector<double> image;
  for (size_t fileIndex = 0; fileIndex != nFitsFiles; ++fileIndex) {
    std::cout << "Reading " << fitsFiles[fileIndex] << " (" << fileIndex << '/'
              << nFitsFiles << ")...\n";
    FitsReader reader(fitsFiles[fileIndex]);
    image.resize(reader.ImageWidth() * reader.ImageHeight());
    reader.Read(image.data());

    for (ModelSource& source : model) {
      for (ModelComponent& component : source) {
        long double l, m;
        aocommon::ImageCoordinates::RaDecToLM<long double>(
            component.PosRA(), component.PosDec(), reader.PhaseCentreRA(),
            reader.PhaseCentreDec(), l, m);
        int x, y;
        aocommon::ImageCoordinates::LMToXY<long double>(
            l, m, reader.PixelSizeX(), reader.PixelSizeY(), reader.ImageWidth(),
            reader.ImageHeight(), x, y);
        component.MSED().AddMeasurement(image[x + y * reader.ImageWidth()],
                                        reader.Frequency());
      }
    }
  }
  model.Save(modelFileOut);
}

int main(int argc, char* argv[]) {
  if (argc < 3)
    std::cout << "Syntax: fitsspectrum <model in> <model out> [images...]\n";
  else
    perform(argv[1], argv[2], const_cast<const char**>(&argv[3]), argc - 3);
}
