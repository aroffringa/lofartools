#include <iostream>

#include <aocommon/uvector.h>

#include <schaapcommon/fitters/nlplfitter.h>

using namespace std;

int main() {
  aocommon::UVector<double> frequencies, fluxes;
  size_t i = 0;
  double freq;
  schaapcommon::fitters::NonLinearPowerLawFitter fitter;
  do {
    std::cout << "Frequency in MHz of measurement " << (i + 1)
              << " (end with 0): ";
    cin >> freq;
    if (freq != 0.0) {
      freq *= 1e6;
      std::cout << "Flux: ";
      double flux;
      cin >> flux;
      frequencies.push_back(freq);
      fluxes.push_back(flux);
      fitter.AddDataPoint(freq, flux);
    }
  } while (freq != 0.0);
  std::vector<float> terms;
  fitter.Fit(terms, 2);
  std::cout << "SI: " << terms[1] << '\n';
  do {
    std::cout << "Evaluate at frequency: (0 to exit)";
    cin >> freq;
    if (freq != 0.0) {
      freq *= 1e6;
      double flux = fitter.Evaluate(freq, terms);
      std::cout << "Flux: " << flux << '\n';
    }
  } while (freq != 0.0);
}
