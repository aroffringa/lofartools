#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>
#include <aocommon/units/angle.h>
#include <aocommon/system.h>
#include <aocommon/image.h>

#include "rmsimage.h"
#include "keywords.h"
#include "stopwatch.h"

#include "facets/ds9facetfile.h"
#include "facets/facetimage.h"

#include "math/dijkstrasplitter.h"
#include "math/gaussianconversions.h"
#include "math/gaussianfitter.h"
#include "math/modelrenderer.h"

#include <iostream>
#include <string>

using namespace aocommon;
using aocommon::units::Angle;

void convolveTo(const std::string& refFilename, const std::string& inpFilename,
                const std::string& destFilename) {
  aocommon::FitsReader refImg(refFilename);
  aocommon::FitsReader inpImg(inpFilename);
  std::cout << "From: " << Angle::ToNiceString(inpImg.BeamMajorAxisRad())
            << " x " << Angle::ToNiceString(inpImg.BeamMinorAxisRad())
            << ", rotation " << Angle::ToNiceString(inpImg.BeamPositionAngle())
            << "\n";
  std::cout << "To: " << Angle::ToNiceString(refImg.BeamMajorAxisRad()) << " x "
            << Angle::ToNiceString(refImg.BeamMinorAxisRad()) << ", rotation "
            << Angle::ToNiceString(refImg.BeamPositionAngle()) << "\n";
  double refSxsx, refSxsy, refSysy;
  math::ToGaussianCovariance(
      refImg.BeamMajorAxisRad(), refImg.BeamMinorAxisRad(),
      refImg.BeamPositionAngle(), refSxsx, refSxsy, refSysy);
  double inpSxsx, inpSxsy, inpSysy;
  math::ToGaussianCovariance(
      inpImg.BeamMajorAxisRad(), inpImg.BeamMinorAxisRad(),
      inpImg.BeamPositionAngle(), inpSxsx, inpSxsy, inpSysy);

  double diffSxsx = refSxsx - inpSxsx, diffSxsy = refSxsy - inpSxsy,
         diffSysy = refSysy - inpSysy;
  double maj, min, rotation;
  math::FromGaussianCovariance(diffSxsx, diffSxsy, diffSysy, maj, min,
                               rotation);
  std::cout << "Convolving with Gaussian of " << Angle::ToNiceString(maj)
            << " x " << Angle::ToNiceString(min) << ", rotation "
            << Angle::ToNiceString(rotation) << ".\n";

  const size_t width = inpImg.ImageWidth(), height = inpImg.ImageHeight();
  Image destData(width, height, 0.0f), inpData(width, height, 0.0f);

  // Calculate normalization factor
  inpData[(width / 2) * height + height / 2] = 1.0;
  ModelRenderer::Restore(destData.Data(), inpData.Data(), width, height,
                         inpImg.BeamMajorAxisRad(), inpImg.BeamMinorAxisRad(),
                         inpImg.BeamPositionAngle(), inpImg.PixelSizeX(),
                         inpImg.PixelSizeY());
  float sum = 0.0;
  for (const float& v : destData) sum += v;

  // Do actual convolution
  destData = 0.0;
  inpImg.Read(inpData.Data());
  ModelRenderer::Restore(destData.Data(), inpData.Data(), width, height, maj,
                         min, rotation, inpImg.PixelSizeX(),
                         inpImg.PixelSizeY());
  for (float& v : destData) v /= sum;

  FitsWriter writer(inpImg);
  writer.SetBeamInfo(refImg.BeamMajorAxisRad(), refImg.BeamMinorAxisRad(),
                     refImg.BeamPositionAngle());
  writer.Write(destFilename, destData.Data());
}

void facet(const std::string& facetFilename, const std::string& fitsFilename) {
  DS9FacetFile f(facetFilename);
  FitsReader reader(fitsFilename);
  DImage model(reader.ImageWidth(), reader.ImageHeight());
  reader.Read(model.Data());
  FacetMap map;
  f.Read(map, reader.PhaseCentreRA(), reader.PhaseCentreDec(),
         reader.PixelSizeX(), reader.PixelSizeY(), reader.ImageWidth(),
         reader.ImageHeight());
  std::cout << "Read " << map.NFacets() << " facets.\n";
  FacetImage image;
  image.Set(reader.ImageWidth(), reader.ImageHeight(), 0.0);
  for (size_t f = 0; f != map.NFacets(); ++f) {
    std::cout << "Facet " << f << "\n";
    image.FillFacet(map[f], f + 1);
  }
  std::cout << "Saving...\n";
  FitsWriter writer(reader);
  writer.SetImageDimensions(image.Width(), image.Height());
  writer.Write("test.fits", image.Data());
}

void madstddev(const std::string& filename) {
  FitsReader reader(filename);
  DImage image(reader.ImageWidth(), reader.ImageHeight());
  reader.Read(image.Data());
  std::cout << image.StdDevFromMAD() << '\n';
}

void rmsImage(const std::string& filename, const std::string& outputFilename) {
  FitsReader reader(filename);
  Image output, input(reader.ImageWidth(), reader.ImageHeight());
  reader.Read(input.Data());
  RMSImage::Make(output, input, 25.0, reader.BeamMajorAxisRad(),
                 reader.BeamMinorAxisRad(), reader.BeamPositionAngle(),
                 reader.PixelSizeX(), reader.PixelSizeY(),
                 aocommon::system::ProcessorCount());
  FitsWriter writer(reader);
  writer.Write(outputFilename, output.Data());
}

void rmsMImage(const std::string& filename, const std::string& outputFilename) {
  FitsReader reader(filename);
  Image output, input(reader.ImageWidth(), reader.ImageHeight());
  reader.Read(input.Data());
  RMSImage::MakeWithNegativityLimit(
      output, input, 25.0, reader.BeamMajorAxisRad(), reader.BeamMinorAxisRad(),
      reader.BeamPositionAngle(), reader.PixelSizeX(), reader.PixelSizeY(),
      aocommon::system::ProcessorCount());
  FitsWriter writer(reader);
  writer.Write(outputFilename, output.Data());
}

void subdivide(const std::string& filename, const std::string& outputFilename,
               size_t maxSubImageSize) {
  FitsReader reader(filename);
  const size_t width = reader.ImageWidth(), height = reader.ImageHeight();
  size_t subImages = (width + maxSubImageSize - 1) / maxSubImageSize,
         averageSubImageSize = width / subImages;
  DijkstraSplitter divisor(width, height);
  Image input(width, height), hScratch(width, height, 0.0),
      vScratch(width, height, 0.0);
  reader.Read(input.Data());
  for (size_t divNr = 1; divNr != subImages; ++divNr) {
    size_t splitStart, splitEnd;
    splitStart = width * divNr / (subImages)-averageSubImageSize / 4;
    splitEnd = width * divNr / (subImages) + averageSubImageSize / 4;
    Stopwatch watch(true);
    std::cout << "Dividing vertically (" << divNr << ")... " << std::flush;
    divisor.DivideVertically(input.Data(), hScratch.Data(), splitStart,
                             splitEnd);
    std::cout << watch.ToString() << '\n';
    std::cout << "Dividing horizontally (" << divNr << ")... " << std::flush;
    divisor.DivideHorizontally(input.Data(), vScratch.Data(), splitStart,
                               splitEnd);
    std::cout << watch.ToString() << '\n';
  }
  Image output(hScratch);
  output += vScratch;
  FitsWriter writer(reader);
  writer.Write(outputFilename, output.Data());
}

void threshold(const std::string& filename, const std::string& outputFilename,
               double threshold) {
  FitsReader reader(filename);
  DImage image(reader.ImageWidth(), reader.ImageHeight());
  reader.Read(image.Data());
  for (double& d : image) {
    d = (d < threshold) ? 0 : 1;
  }
  FitsWriter writer(reader);
  writer.Write(outputFilename, image.Data());
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cout
        << "Syntax: aoop <operation> [operation parameters]\n\n"
           "Operations:\n"
           "\tconvolve-to <ref img> <inp img> <outp img>\n"
           "\t   Convolves inp img with a Gaussian to match the beam size of "
           "the ref img\n"
           "\tfacet <ds9file> <fitsfile>\n"
           "\tmad-stddev <image>\n"
           "\t  Computes standard deviation from the MAD\n"
           "\trmsimage <image> <output>\n"
           "\t  Make an RMS background image\n"
           "\trmsmimage <image> <output>\n"
           "\t  Make an RMS/minimum pixel background image\n"
           "\twinmin <image> <output>\n"
           "\t  Make a minimum pixel background image\n"
           "\tsigimage <image> <output>\n"
           "\t  Make a significance image\n"
           "\tmakemask <image> <output>\n"
           "\t  Sets every non-zero pixel to 1\n"
           "\tset <image> <value>\n"
           "\t  Overwrites the image and sets all values to given value.\n"
           "\tset-delta\n"
           "\tsubdivide\n"
           "\tfitgaus <image>\n"
           "\t  Perform a Gaussian 'psf' fit.\n"
           "\ttrim <image> <output> <width> <height>\n"
           "\t  Trim the image.\n"
           "\tthreshold <input> <output> <threshold>\n"
           "\t  Threshold the input: values < threshold are set to 0, values > "
           "threshold to 1.\n"
           "\t  Can be used to make a mask.\n";
  } else {
    std::string operation(argv[1]);

    if (operation == "convolve-to") {
      convolveTo(argv[2], argv[3], argv[4]);
    } else if (operation == "facet") {
      facet(argv[2], argv[3]);
    } else if (operation == "mad-stddev") {
      madstddev(argv[2]);
    } else if (operation == "rmsimage") {
      rmsImage(argv[2], argv[3]);
    } else if (operation == "rmsmimage") {
      rmsMImage(argv[2], argv[3]);
    } else if (operation == "sigimage") {
      FitsReader reader(argv[2]);
      Image output, input(reader.ImageWidth(), reader.ImageHeight());
      reader.Read(input.Data());
      RMSImage::Make(output, input, 25.0, reader.BeamMajorAxisRad(),
                     reader.BeamMinorAxisRad(), reader.BeamPositionAngle(),
                     reader.PixelSizeX(), reader.PixelSizeY(),
                     aocommon::system::ProcessorCount());
      FitsWriter writer(reader);
      for (size_t i = 0; i != output.Size(); ++i)
        output[i] = input[i] / output[i];
      writer.Write(argv[3], output.Data());
    } else if (operation == "winmin") {
      FitsReader reader(argv[2]);
      Image output, input(reader.ImageWidth(), reader.ImageHeight());
      reader.Read(input.Data());
      double beamInPixels =
          std::max(reader.BeamMajorAxisRad() / reader.PixelSizeX(), 1.0);
      RMSImage::SlidingMinimum(output, input, 25.0 * beamInPixels,
                               aocommon::system::ProcessorCount());
      for (float& v : output) v = std::abs(v);
      FitsWriter writer(reader);
      writer.Write(argv[3], output.Data());
    } else if (operation == "makemask") {
      FitsReader reader(argv[2]);
      Image image(reader.ImageWidth(), reader.ImageHeight());
      reader.Read(image.Data());
      for (float& v : image)
        if (v != 0.0) v = 1.0;
      FitsWriter writer(reader);
      writer.Write(argv[3], image.Data());
    } else if (operation == "set") {
      FitsReader reader(argv[2]);
      Keywords keywords(reader);
      Image image(reader.ImageWidth(), reader.ImageHeight(), atof(argv[3]));
      FitsWriter writer(reader);
      keywords.Set(writer);
      writer.Write(argv[2], image.Data());
    } else if (operation == "set-delta") {
      FitsReader reader(argv[2]);
      Keywords keywords(reader);
      Image image(reader.ImageWidth(), reader.ImageHeight(), 0.0);
      image[reader.ImageWidth() / 2 +
            (reader.ImageHeight() / 2) * reader.ImageWidth()] = atof(argv[3]);
      FitsWriter writer(reader);
      keywords.Set(writer);
      writer.Write(argv[2], image.Data());
    } else if (operation == "subdivide") {
      subdivide(argv[2], argv[3], atoi(argv[4]));
    } else if (operation == "trim") {
      std::string infilename(argv[2]);
      std::string outfilename(argv[3]);
      size_t width = atoi(argv[4]), height = atoi(argv[5]);
      FitsReader reader(infilename);
      Image image(reader.ImageWidth(), reader.ImageHeight());
      reader.Read(image.Data());
      image = image.Trim(width, height);
      FitsWriter writer(reader);
      writer.SetImageDimensions(width, height);
      writer.Write(outfilename, image.Data());
    } else if (operation == "threshold") {
      threshold(argv[2], argv[3], atof(argv[4]));
    } else if (operation == "fitgaus" || operation == "fitgauswrite") {
      int argi = 2;
      double major = 0.0, boxFactor = 10.0;
      bool hasInitial = false;
      while (argv[argi][0] == '-') {
        std::string p(argv[argi]);
        if (p == "-initial") {
          major = Angle::Parse(argv[argi + 1], "initial value for beam",
                               Angle::kArcseconds);
          hasInitial = true;
          argi += 2;
        } else if (p == "-box") {
          boxFactor = atoi(argv[argi + 1]);
          argi += 2;
        } else
          ++argi;
      }
      FitsReader reader(argv[argi]);
      std::cout << "Keywords: "
                << Angle::ToNiceString(reader.BeamMajorAxisRad()) << " x "
                << Angle::ToNiceString(reader.BeamMinorAxisRad())
                << ", rotation "
                << Angle::ToNiceString(reader.BeamPositionAngle()) << '\n';
      if (!hasInitial) {
        major = reader.BeamMajorAxisRad();
      }
      std::cout << "Using for initial value: " << Angle::ToNiceString(major)
                << '\n';

      Image image(reader.ImageWidth(), reader.ImageHeight());
      double estBeam = major / reader.PixelSizeX();
      reader.Read(image.Data());
      GaussianFitter fitter;
      double beamMaj = 0.0, beamMin = 0.0, beamPA = 0.0, x = 0.0, y = 0.0,
             amplitude = 0.0;
      fitter.Fit2DGaussianCentred(image.Data(), reader.ImageWidth(),
                                  reader.ImageHeight(), estBeam, beamMaj,
                                  beamMin, beamPA, boxFactor, true);
      std::cout << "Centered fit: "
                << Angle::ToNiceString(beamMaj * reader.PixelSizeX()) << " x "
                << Angle::ToNiceString(beamMin * reader.PixelSizeX())
                << ", rotation " << Angle::ToNiceString(beamPA) << '\n';
      if (operation == "fitgauswrite") {
        FitsWriter writer(reader);
        writer.SetBeamInfo(beamMaj * reader.PixelSizeX(),
                           beamMin * reader.PixelSizeX(), beamPA);
        writer.Write(argv[2], image.Data());
      }

      double circularBeam = estBeam;
      fitter.Fit2DCircularGaussianCentred(image.Data(), reader.ImageWidth(),
                                          reader.ImageHeight(), circularBeam,
                                          boxFactor);
      std::cout << "Circular centered fit: "
                << Angle::ToNiceString(circularBeam * reader.PixelSizeX())
                << '\n';

      x = reader.ImageWidth() / 2;
      y = reader.ImageHeight() / 2;
      fitter.Fit2DGaussianFull(image.Data(), reader.ImageWidth(),
                               reader.ImageHeight(), amplitude, x, y, beamMaj,
                               beamMin, beamPA);
      std::cout << "Shape, position and amplitude fit: "
                << Angle::ToNiceString(beamMaj * reader.PixelSizeX()) << " x "
                << Angle::ToNiceString(beamMin * reader.PixelSizeX())
                << ", rotation " << Angle::ToNiceString(beamPA)
                << ", position (" << x << ", " << y << "), amplitude "
                << amplitude << '\n';

      x = reader.ImageWidth() / 2;
      y = reader.ImageHeight() / 2;
      double background = 0.0;
      fitter.Fit2DGaussianFull(image.Data(), reader.ImageWidth(),
                               reader.ImageHeight(), amplitude, x, y, beamMaj,
                               beamMin, beamPA, &background);
      std::cout << "Shape, position, amplitude and background fit: "
                << Angle::ToNiceString(beamMaj * reader.PixelSizeX()) << " x "
                << Angle::ToNiceString(beamMin * reader.PixelSizeX())
                << ", rotation " << Angle::ToNiceString(beamPA)
                << ", position (" << x << ", " << y << "), amplitude "
                << amplitude << " (+" << background << ")\n";
    } else {
      std::cerr << "Unknown operation.\n";
    }
  }
}
