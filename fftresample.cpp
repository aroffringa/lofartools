#include "fftresampler.h"

#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>

#include <aocommon/system.h>
#include <aocommon/uvector.h>

#include <iostream>

int main(int argc, char* argv[]) {
  if (argc < 5) {
    std::cout << "Syntax: fftresample [-f] <input> <output> <width> <height>\n"
                 "-f : when enlarging the image, make sure total flux remains "
                 "the same.\n";
  } else {
    bool keepFluxEqual = false;
    int argi = 1;
    while (argi < argc && argv[argi][0] == '-') {
      std::string p = &argv[argi][1];
      if (p == "f")
        keepFluxEqual = true;
      else
        throw std::runtime_error("Bad option");
      ++argi;
    }

    aocommon::FitsReader reader(argv[argi]);
    const size_t width = reader.ImageWidth(), height = reader.ImageHeight();
    const size_t outWidth = atoi(argv[argi + 2]),
                 outHeight = atoi(argv[argi + 3]);
    float* inputData =
        reinterpret_cast<float*>(fftwf_malloc(width * height * sizeof(float)));
    float* outputData = reinterpret_cast<float*>(
        fftwf_malloc(outWidth * outHeight * sizeof(float)));
    FFTResampler resampler(width, height, outWidth, outHeight,
                           aocommon::system::ProcessorCount());
    reader.Read(inputData);

    resampler.Resample(inputData, outputData);
    fftwf_free(inputData);

    if (keepFluxEqual && outWidth > width && outHeight > height) {
      float factor = float(width * height) / float(outWidth * outHeight);
      float* ptr = outputData;
      for (size_t y = 0; y != outHeight; ++y) {
        for (size_t x = 0; x != outWidth; ++x) {
          *ptr *= factor;
          ++ptr;
        }
      }
    }

    aocommon::FitsWriter writer(reader);
    writer.SetImageDimensions(outWidth, outHeight,
                              reader.PixelSizeX() * width / outWidth,
                              reader.PixelSizeY() * height / outHeight);
    writer.Write(argv[argi + 1], outputData);

    fftwf_free(outputData);
  }
}
