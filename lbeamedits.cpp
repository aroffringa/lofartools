#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/measures/TableMeasures/ArrayMeasColumn.h>

#include <casacore/measures/Measures/MEpoch.h>

#include <EveryBeam/load.h>
#include <EveryBeam/pointresponse/pointresponse.h>

#include "model/model.h"

#include <aocommon/matrix2x2.h>
#include <aocommon/banddata.h>
#include "progressbar.h"

#include <iostream>
#include <set>

using aocommon::MC2x2F;

int main(int argc, char* argv[]) {
  int argi = 1;
  double threshold = 0.0, rangeMin = 0.0, rangeMax = 0.0;
  std::string outputFilename;
  while (argi < argc && argv[argi][0] == '-') {
    std::string p(&argv[argi][1]);
    if (p == "threshold") {
      ++argi;
      threshold = atof(argv[argi]);
    } else if (p == "range") {
      rangeMin = atof(argv[argi + 1]);
      rangeMax = atof(argv[argi + 2]);
      argi += 2;
    } else if (p == "save") {
      ++argi;
      outputFilename = argv[argi];
    } else
      throw std::runtime_error("Unknown parameter given");
    ++argi;
  }
  if (argi + 2 > argc) {
    std::cout << "syntax: lbeamedits [-save <newmodel>] [-threshold <app "
                 "flux>] <model> <ms>\n";
    return 0;
  }
  Model model(argv[argi]);
  casacore::MeasurementSet ms(argv[argi + 1]);

  aocommon::BandData band(ms.spectralWindow());

  std::unique_ptr<everybeam::telescope::Telescope> telescope =
      everybeam::Load(ms, everybeam::Options());
  /*
  std::set<size_t> antennaSet;
  for (size_t i = 0; i != aTable.nrow(); ++i) {
    if (antNameColumn(i).substr(0, 2) == "CS" ||
        antNameColumn(i).substr(0, 2) == "RS")
      antennaSet.emplace(i);
  }
  std::cout << "Using " << antennaSet.size() << " antennas.\n";
  */

  casacore::ScalarColumn<double> timeColumn(
      ms, ms.columnName(casacore::MeasurementSet::TIME));
  double time = timeColumn(ms.nrow() / 2);
  casacore::MEpoch timeEpoch(casacore::Quantity(time, "s"));

  if (threshold != 0.0 || rangeMin != 0.0 || rangeMax != 0.0) {
    if (threshold != 0.0) {
      if (rangeMin == 0.0)
        rangeMin = threshold;
      else
        rangeMin = std::min(rangeMin, threshold);
    }
    ProgressBar progress("Thresholding");
    Model newModel;
    for (size_t i = 0; i != model.SourceCount(); ++i) {
      const ModelSource& source = model.Source(i);

      MC2x2F stationGains(MC2x2F::Zero());
      std::unique_ptr<everybeam::pointresponse::PointResponse> pointResponse =
          telescope->GetPointResponse(time);
      pointResponse->ResponseAllStations(
          everybeam::BeamMode::kFull,
          aocommon::DubiousComplexPointerCast(stationGains), source.MeanRA(),
          source.MeanDec(), band.CentreFrequency(), 0);
      stationGains /= float(telescope->GetNrStations());

      double stokesIFlux = source.TotalFlux(band.CentreFrequency(),
                                            aocommon::Polarization::StokesI);
      MC2x2F flux(stokesIFlux, 0.0, 0.0, stokesIFlux);
      flux = stationGains.Multiply(flux).MultiplyHerm(stationGains);
      stokesIFlux = std::abs(flux.Get(0) + flux.Get(3)) * 0.5;
      if (stokesIFlux >= rangeMin &&
          (stokesIFlux <= rangeMax || rangeMax == 0.0))
        newModel.AddSource(source);
      progress.SetProgress(i, model.SourceCount());
    }
    model = std::move(newModel);
  }

  if (!outputFilename.empty()) {
    std::cout << "Writing model with " << model.SourceCount() << " sources.\n";
    model.Save(outputFilename);
  }
}
