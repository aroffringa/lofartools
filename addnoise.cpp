#include <iostream>
#include <random>

#include <casacore/ms/MeasurementSets/MeasurementSet.h>

#include <casacore/tables/Tables/ArrayColumn.h>

using namespace casacore;

int main(int argc, char **argv) {
  if (argc < 2) {
    std::cout
        << "Usage: addnoise <sigma> <ms>\n"
           "Flags all correlations of baselines that should not be used.\n";
  } else {
    const double sigma = atof(argv[1]);
    MeasurementSet ms(argv[2], Table::Update);

    ArrayColumn<casacore::Complex> dataColumn(ms, "DATA");
    if (ms.nrow() == 0) throw std::runtime_error("Table has no rows (no data)");
    IPosition dataShape = dataColumn.shape(0);
    Array<Complex> dataArray(dataShape);

    std::mt19937 rnd_generator;
    std::normal_distribution normal_distribution(0.0, sigma);
    for (size_t rowIndex = 0; rowIndex != ms.nrow(); ++rowIndex) {
      dataColumn.get(rowIndex, dataArray);

      for (Array<Complex>::contiter value_iter = dataArray.cbegin();
           value_iter != dataArray.cend(); ++value_iter) {
        *value_iter += Complex(normal_distribution(rnd_generator),
                               normal_distribution(rnd_generator));
      }
      dataColumn.put(rowIndex, dataArray);
    }
  }

  return 0;
}
