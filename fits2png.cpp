#include "colormap.h"
#include "pngfile.h"

#include <aocommon/fits/fitsreader.h>
#include <aocommon/uvector.h>
#include <aocommon/image.h>

#include "model/model.h"

#include <iostream>
#include <vector>

using namespace aocommon;

double signedSqrt(double val) {
  if (val >= 0.0)
    return std::sqrt(val);
  else
    return -std::sqrt(-val);
}

struct Color {
  unsigned char r, g, b;
};

int main(int argc, char* argv[]) {
  if (argc < 6) {
    std::cout
        << "Syntax: fits2png [options] <map> <fitsfile> <min> <max> <pngfile>\n"
           "\n"
           "Options:\n"
           "-mark <model>\n"
           "-log\n"
           "-sqrt\n"
           "-trim <width> <height>\n"
           "-cut <x> <y> <width> <height>\n"
           "-brightness-image <min> <max> <name>\n"
           "-brightness-sqrt\n"
           "  Combines a primary image (e.g. spectral index data) with "
           "brightness information.\n";
    return 0;
  }
  int argi = 1;
  size_t cutX = 0, cutY = 0;
  size_t trimWidth = 0, trimHeight = 0;
  bool logarithmic = false, sqrtScale = false, cut = false;
  std::string brightnessImageFilename, markModelFilename;
  bool brightnessSqrt = false;
  double bMin = 0.0, bMax = 0.0;
  while (argv[argi][0] == '-') {
    std::string p(&argv[argi][1]);
    if (p == "trim") {
      trimWidth = atoi(argv[argi + 1]);
      trimHeight = atoi(argv[argi + 2]);
      argi += 2;
    } else if (p == "cut") {
      cut = true;
      cutX = atoi(argv[argi + 1]);
      cutY = atoi(argv[argi + 2]);
      trimWidth = atoi(argv[argi + 3]);
      trimHeight = atoi(argv[argi + 4]);
      argi += 4;
    } else if (p == "log") {
      logarithmic = true;
    } else if (p == "mark") {
      ++argi;
      markModelFilename = argv[argi];
    } else if (p == "sqrt") {
      sqrtScale = true;
    } else if (p == "brightness-image") {
      bMin = atof(argv[argi + 1]);
      bMax = atof(argv[argi + 2]);
      brightnessImageFilename = argv[argi + 3];
      argi += 3;
    } else if (p == "brightness-sqrt") {
      brightnessSqrt = true;
    } else
      throw std::runtime_error("Bad parameter");
    ++argi;
  }

  std::string mapName = argv[argi], pngFilename(argv[argi + 4]);
  FitsReader reader(argv[argi + 1], false);
  double minVal = atof(argv[argi + 2]), maxVal = atof(argv[argi + 3]);

  size_t width = reader.ImageWidth(), height = reader.ImageHeight();

  std::cout << "Reading...\n";

  Image image(width, height);
  reader.Read(image.Data());

  Image brightnessImage;
  if (!brightnessImageFilename.empty()) {
    FitsReader bReader(brightnessImageFilename);
    if (bReader.ImageWidth() != width || bReader.ImageHeight() != height)
      throw std::runtime_error("Brightness image has wrong dimensions");
    brightnessImage = Image(width, height);
    bReader.Read(brightnessImage.Data());
  }

  if (cut) {
    Image scratch(trimWidth, trimHeight);
    Image::TrimBox(scratch.Data(), cutX, cutY, trimWidth, trimHeight,
                   image.Data(), width, height);
    image = scratch;
    if (!brightnessImage.Empty()) {
      Image::TrimBox(scratch.Data(), cutX, cutY, trimWidth, trimHeight,
                     brightnessImage.Data(), width, height);
      brightnessImage = scratch;
    }
    width = trimWidth;
    height = trimHeight;
  } else if (trimWidth != 0) {
    Image scratch(trimWidth, trimHeight);
    Image::Trim(scratch.Data(), trimWidth, trimHeight, image.Data(), width,
                height);
    image = scratch;
    if (!brightnessImage.Empty()) {
      Image::Trim(scratch.Data(), trimWidth, trimHeight, brightnessImage.Data(),
                  width, height);
      brightnessImage = scratch;
    }
    width = trimWidth;
    height = trimHeight;
  }

  std::vector<Color> colorImage(width * height);
  std::cout << "Rendering...\n";

  std::unique_ptr<ColorMap> map(ColorMap::CreateColorMap(mapName));

  for (size_t i = 0; i != image.Size(); ++i) {
    double value = image[i];
    // scale to -1 to 1
    double normValue;
    if (logarithmic)
      normValue =
          2.0 * std::log(std::fabs(value / minVal)) / log(maxVal / minVal) -
          1.0;
    else if (sqrtScale) {
      normValue = 2.0 * (signedSqrt(value) - signedSqrt(minVal)) /
                      (signedSqrt(maxVal) - signedSqrt(minVal)) -
                  1.0;
    } else
      normValue = 2.0 * (value - minVal) / (maxVal - minVal) - 1.0;
    if (!std::isfinite(normValue))
      normValue = -1.0;
    else if (normValue > 1.0)
      normValue = 1.0;
    else if (normValue < -1.0)
      normValue = -1.0;
    unsigned char r, g, b;
    map->Convert(normValue, r, g, b);

    if (!brightnessImage.Empty()) {
      double brightn = brightnessImage[i];
      double normB;
      if (brightnessSqrt)
        normB = (signedSqrt(brightn) - signedSqrt(bMin)) /
                (signedSqrt(bMax) - signedSqrt(bMin));
      else
        normB = (brightn - bMin) / (bMax - bMin);
      if (normB > 1.0)
        normB = 1.0;
      else if (normB < 0.0)
        normB = 0.0;
      // r = round((1.0-normB) * 255.) + normB * double(r);
      // g = round((1.0-normB) * 255.) + normB * double(g);
      // b = round((1.0-normB) * 255.) + normB * double(b);
      r = normB * double(r);
      g = normB * double(g);
      b = normB * double(b);
    }

    colorImage[i].r = r;
    colorImage[i].g = g;
    colorImage[i].b = b;
  }

  if (!markModelFilename.empty()) {
    Model model(markModelFilename);
    for (const ModelSource& source : model) {
      for (const ModelComponent& component : source) {
        long double l, m;
        ImageCoordinates::RaDecToLM<long double>(
            component.PosRA(), component.PosDec(), reader.PhaseCentreRA(),
            reader.PhaseCentreDec(), l, m);
        int x, y;
        ImageCoordinates::LMToXY<long double>(l, m, reader.PixelSizeX(),
                                              reader.PixelSizeY(), width,
                                              height, x, y);
        if (component.SED().FluxAtFrequency(
                component.SED().ReferenceFrequencyHz(),
                aocommon::Polarization::StokesI) > 0.0) {
          if (component.Type() == ModelComponent::PointSource) {
            if (x >= 0 && y >= 0 && x < int(width) && y < int(height)) {
              colorImage[x + y * width] = Color{0, 0, 255};
            }
          } else if (component.Type() == ModelComponent::GaussianSource) {
            // For now only circular sources are supported
            double linewidth = 1.0;
            double radius = 0.5 * component.MajorAxis() / reader.PixelSizeX();
            double outerRadiusSq =
                (radius + linewidth * 0.5) * (radius + linewidth * 0.5);
            double innerRadiusSq =
                (radius - linewidth * 0.5) * (radius - linewidth * 0.5);
            int radiusCeil = ceil(radius);
            int startX = x - radiusCeil, startY = y - radiusCeil,
                endX = x + radiusCeil + 1, endY = y + radiusCeil + 1;
            for (int yi = startY; yi != endY; ++yi) {
              for (int xi = startX; xi != endX; ++xi) {
                if (xi >= 0 && yi >= 0 && xi < int(width) && yi < int(height)) {
                  int dy = yi - y, dx = xi - x, nrm = dx * dx + dy * dy;
                  if (nrm < outerRadiusSq && nrm > innerRadiusSq) {
                    colorImage[xi + yi * width] = Color{0, 0, 255};
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  std::cout << "Writing " << pngFilename << "...\n";
  std::vector<Color>::const_iterator colorImagePtr = colorImage.begin();
  PngFile file(pngFilename, width, height);
  file.BeginWrite();
  for (size_t y = 0; y < height; ++y) {
    for (size_t x = 0; x < width; ++x) {
      file.PlotPixel(x, height - 1 - y, colorImagePtr->r, colorImagePtr->g,
                     colorImagePtr->b, 255);
      ++colorImagePtr;
    }
  }
  file.Close();
}
