#include <iostream>

#include <aocommon/uvector.h>

#include "deconvolution/lsdeconvolution.h"

#include "fitsreader.h"
#include "fitswriter.h"

int main(int argc, char* argv[]) {
  if (argc != 6) {
    std::cout << "Syntax: <dirty> <psf> <mask> <out model> <out residual>\n";
    return -1;
  }

  FitsReader dirtyReader(argv[1]), psfReader(argv[2]), maskReader(argv[3]);

  const size_t width = dirtyReader.ImageWidth(),
               height = dirtyReader.ImageHeight();

  aocommon::UVector<double> dirty(width * height), psf(width * height),
      maskImage(width * height), model(width * height, 0.0);
  aocommon::UVector<bool> mask(width * height);

  dirtyReader.Read(dirty.data());
  psfReader.Read(psf.data());
  maskReader.Read(maskImage.data());

  if (maskReader.ImageWidth() != width || maskReader.ImageHeight() != height)
    throw std::runtime_error(
        "Specified Fits file mask did not have same dimensions as output "
        "image!");
  aocommon::UVector<float> maskData(width * height);
  maskReader.Read(maskData.data());
  for (size_t i = 0; i != width * height; ++i) mask[i] = maskData[i] != 0.0;

  bool reachedMajorThreshold;
  LSDeconvolution deconvolution;
  deconvolution.SetCleanMask(mask.data());
  deconvolution.ExecuteMajorIteration(dirty.data(), model.data(), psf.data(),
                                      width, height, reachedMajorThreshold);

  FitsWriter writer(dirtyReader);
  writer.Write(argv[4], model.data());
  writer.Write(argv[5], dirty.data());
}
