#include <wcs.h>
#include <wcshdr.h>
#include <fitsio.h>

#include <iostream>
#include <vector>

#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>
#include <aocommon/image.h>
#include <aocommon/imagecoordinates.h>

#include "render/interpolatingrenderer.h"

int main(int argc, char* argv[]) {
  fitsfile* fits_file;
  const char* input_filename = argv[1];
  const char* template_filename = argv[2];
  const char* output_filename = argv[3];
  int status = 0;
  fits_open_file(&fits_file, input_filename, READONLY, &status);
  int hdu_type;
  fits_movabs_hdu(fits_file, 1, &hdu_type, &status);
  char* header;
  int nkeyrec = 0;
  fits_hdr2str(fits_file, 1, nullptr, 0, &header, &nkeyrec, &status);

  int naxis = 0;
  fits_get_img_dim(fits_file, &naxis, &status);
  if (naxis != 2) throw std::runtime_error("NAxis in image != 2");

  std::vector<long> naxes(naxis);
  fits_get_img_size(fits_file, naxis, naxes.data(), &status);
  const size_t width = naxes[0];
  const size_t height = naxes[1];

  int n_rejects = 0;
  int n_wcs = 0;
  wcsprm* wcs = nullptr;
  wcspih(header, nkeyrec, WCSHDR_all, 2, &n_rejects, &n_wcs, &wcs);
  fits_free_memory(header, &status);

  bool lon_is_galactic = std::string(wcs->ctype[0]).substr(0, 4) == "GLON";
  bool lat_is_galactic = std::string(wcs->ctype[1]).substr(0, 4) == "GLAT";
  bool lon_is_equatorial = std::string(wcs->ctype[0]).substr(0, 2) == "RA";
  bool lat_is_equatorial = std::string(wcs->ctype[1]).substr(0, 3) == "DEC";
  if (lon_is_galactic && lat_is_galactic) {
    std::cout << "Converting Galactic coordinates to equatorial.\n";
    const double kGalacticPoleInJ2000[2] = {192.8595, +27.1283};
    const double kJ2000PoleInGalactic[2] = {122.9319, +27.1283};
    const int result =
        wcsccs(wcs, kGalacticPoleInJ2000[0], kGalacticPoleInJ2000[1],
               kJ2000PoleInGalactic[0], "RA", "DEC", "FK5", 2000.0, nullptr);
    if (result) {
      std::cout << "Conversion failed.\n";
    }
  } else if (!lon_is_equatorial || !lat_is_equatorial) {
    std::cout << "Unsupported coordinate system.\n";
  }

  const size_t n_elements = 2;
  const size_t n_coord = width * height;
  std::vector<double> coordinates;
  coordinates.reserve(n_coord * n_elements);
  for (size_t x = 0; x != width; ++x) {
    for (size_t y = 0; y != height; ++y) {
      coordinates.emplace_back(y);
      coordinates.emplace_back(x);
    }
  }
  std::vector<double> image_coord(n_coord * n_elements);
  std::vector<double> phi(n_coord);
  std::vector<double> theta(n_coord);
  std::vector<double> world(n_coord * n_elements);
  std::vector<int> stat(n_coord);
  wcsp2s(wcs, n_coord, n_elements, coordinates.data(), image_coord.data(),
         phi.data(), theta.data(), world.data(), stat.data());
  size_t index = 0;
  for (size_t x = 0; x != width; ++x) {
    for (size_t y = 0; y != height; ++y) {
      ++index;
    }
  }

  std::vector<long> firstPixel(naxis);
  for (int i = 0; i != naxis; ++i) firstPixel[i] = 1;
  if (naxis > 2) firstPixel[2] = index + 1;

  aocommon::Image input_image(width, height, 0.0f);
  fits_read_pix(fits_file, TFLOAT, firstPixel.data(), width * height, nullptr,
                input_image.Data(), nullptr, &status);

  std::cout << "Rendering.\n";
  InterpolatingRenderer renderer(128);
  aocommon::FitsReader template_file(template_filename);
  const double dl = template_file.PixelSizeX() * 8;
  const double dm = template_file.PixelSizeY() * 8;
  aocommon::Image output_image(template_file.ImageWidth(),
                               template_file.ImageHeight());
  for (size_t i = 0; i != input_image.Size(); ++i) {
    const float value = input_image[i];
    if (stat[i] == 0.0 && std::isfinite(value) && value != 0.0) {
      const double ra = world[i * 2 + wcs->lng] * (M_PI / 180.0);
      const double dec = world[i * 2 + wcs->lat] * (M_PI / 180.0);
      double l, m;
      aocommon::ImageCoordinates::RaDecToLM(
          ra, dec, template_file.PhaseCentreRA(),
          template_file.PhaseCentreDec(), l, m);
      double x, y;
      aocommon::ImageCoordinates::LMToXYfloat(
          l, m, dl, dm, template_file.ImageWidth(), template_file.ImageHeight(),
          x, y);
      if (x >= 0.0 && x < width && y >= 0.0 && y < height) {
        renderer.RenderWindowedSource(output_image.Data(), output_image.Width(),
                                      output_image.Height(), value, x, y);
      }
    }
  }

  wcsvfree(&n_wcs, &wcs);
  fits_close_file(fits_file, &status);

  aocommon::FitsWriter writer(template_file);
  writer.SetImageDimensions(template_file.ImageWidth(),
                            template_file.ImageHeight(), dl, dm);
  writer.Write(output_filename, output_image.Data());
}
