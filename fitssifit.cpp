#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <limits>

#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>
#include <aocommon/uvector.h>
#include <aocommon/dynamicfor.h>
#include <aocommon/threadpool.h>

#include <schaapcommon/fitters/nlplfitter.h>

#include "progressbar.h"

void performSIFit(const std::string& outputFilename,
                  const std::vector<std::string>& inputFilenames,
                  double threshold, double blankValue) {
  std::unique_ptr<aocommon::FitsReader> reader;
  std::vector<aocommon::UVector<double>> images;
  aocommon::UVector<double> frequencies;
  for (std::vector<std::string>::const_iterator inp = inputFilenames.begin();
       inp != inputFilenames.end(); ++inp) {
    std::cout << "Reading " << *inp << '\n';
    reader.reset(new aocommon::FitsReader(*inp));
    images.push_back(aocommon::UVector<double>());
    aocommon::UVector<double>& data = images.back();
    data.resize(reader->ImageWidth() * reader->ImageHeight());
    reader->Read(data.data());

    frequencies.push_back(reader->Frequency());
  }

  aocommon::UVector<double> outputImage(images[0].size());

  ProgressBar progress("Fitting");
  aocommon::DynamicFor<size_t> loop;
  loop.Run(0, reader->ImageHeight(), [&](size_t y, size_t) {
    size_t index = y * reader->ImageWidth();
    for (size_t x = 0; x != reader->ImageWidth(); ++x) {
      schaapcommon::fitters::NonLinearPowerLawFitter fitter;
      bool isZero = true;
      double mean = 0.0;
      size_t count = 0;
      for (size_t i = 0; i != images.size(); ++i) {
        if (std::isfinite(images[i][index])) {
          if (images[i][index] != 0.0) isZero = false;
          fitter.AddDataPoint(frequencies[i], images[i][index]);
          mean += images[i][index];
          ++count;
        }
      }

      float si = -0.7, f = 1.0;
      if (isZero || mean / count < threshold)
        si = blankValue;
      else
        fitter.Fit(si, f);

      outputImage[index] = si;

      ++index;
    }
    progress.SetProgress(y + 1, reader->ImageHeight());
  });
  aocommon::FitsWriter writer(*reader);
  writer.Write(outputFilename, outputImage.data());
}

int main(int argc, char* argv[]) {
  if (argc <= 3) {
    std::cout << "syntax:\n\tfitssifit [options] <output> <input1> <input2> "
                 "[<input3> ..]\n"
                 "options:\n"
                 "  -t <threshold>\n"
                 "  -blank <value>\n";
  } else {
    int argi = 1;
    double threshold = 0.0,
           blankValue = std::numeric_limits<double>::quiet_NaN();
    while (argv[argi][0] == '-') {
      std::string p = &argv[argi][1];
      if (p == "t") {
        ++argi;
        threshold = atof(argv[argi]);
      } else if (p == "blank") {
        ++argi;
        blankValue = atof(argv[argi]);
      } else
        throw std::runtime_error("Unknown value");
      ++argi;
    }

    std::string output(argv[argi]);
    std::vector<std::string> inputs;

    for (int i = argi + 1; i != argc; ++i) inputs.push_back(argv[i]);

    performSIFit(output, inputs, threshold, blankValue);
  }
  return 0;
}
