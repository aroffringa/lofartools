#include <filesystem>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>

#include <casacore/tables/Tables/ArrayColumn.h>
#include <casacore/tables/Tables/Table.h>
#include <casacore/tables/DataMan/DataManager.h>

std::string ToString(casacore::DataType type) {
  std::ostringstream str;
  str << type;
  return str.str();
}

size_t BitCount(casacore::DataType type) {
  if (type == casacore::TpBool)
    return 1;
  else
    return SizeOfType(type) * 8;
}

size_t FileSize(const std::string& filename) {
  std::ifstream file(filename);
  file.seekg(0, std::ios::end);
  return file.tellg();
}

std::string SizeToString(size_t size) {
  std::stringstream str;
  str << std::setprecision(4);
  if (size > 1024L * 1024L * 1024L) {
    str << double(size) / (1024.0 * 1024.0 * 1024.0) << " GB";
  } else if (size > 1024L * 1024L) {
    str << double(size) / (1024.0 * 1024.0) << " MB";
  } else if (size > 1024L) {
    str << double(size) / (1024.0) << " KB";
  } else {
    str << size << " B";
  }
  return str.str();
}

struct FileInfo {
  std::vector<std::string> columns;
  size_t volume;
};

int main(int argc, char* argv[]) {
  if (argc < 1) {
    std::cout << "Syntax: volumes <obs.ms>\n";
    return 1;
  }
  const std::string ms_filename = argv[1];

  casacore::Table table(ms_filename);
  casacore::TableDesc table_description = table.actualTableDesc();
  const std::vector<casacore::String> column_names =
      table_description.columnNames().tovector();
  size_t total_size = 0;
  size_t uncompressed_size = 0;
  std::map<std::string, FileInfo> info_per_file;
  for (const std::string& column_name : column_names) {
    std::cout << "Column: " << column_name << '\n';
    const casacore::ColumnDesc& column_description =
        table_description.columnDesc(column_name);
    const casacore::DataType type = column_description.dataType();
    std::cout << "Type: " << ToString(type) << " (" << BitCount(type) << ")\n";
    size_t size;
    if (column_description.isArray()) {
      const casacore::TableColumn array_column(table, column_name);
      const bool fixed_shape = column_description.isFixedShape();
      if (fixed_shape) {
        const size_t n_values = column_description.shape().product();
        std::cout << "Fixed shape with " << n_values << " values.\n";
        size_t size_per_row = (n_values * BitCount(type) + 7) / 8;
        size = size_per_row * table.nrow();
        std::cout << "Raw size: " << SizeToString(size) << "\n";
      } else {
        size_t n_values = 0;
        for (size_t i = 0; i != table.nrow(); ++i) {
          if (array_column.isDefined(i))
            n_values += array_column.shape(i).product();
        }
        std::cout
            << "Array column with variable shape. Average values per row: "
            << n_values / table.nrow() << '\n';
        size = (n_values * BitCount(type) + 7) / 8;
        std::cout << "Raw size: " << SizeToString(size) << "\n";
      }
    } else {
      size = table.nrow() * (BitCount(type) + 7) / 8;
      std::cout << "Raw size: " << SizeToString(size) << "\n";
    }
    total_size += size;
    std::cout << "Manager group: " << column_description.dataManagerGroup()
              << '\n';
    std::cout << "Manager type: " << column_description.dataManagerType()
              << '\n';
    bool is_incremental =
        column_description.dataManagerType() == "IncrementalStMan";
    bool is_dysco = column_description.dataManagerType() == "DyscoStMan";
    if (!is_incremental && !is_dysco) uncompressed_size += size;
    const casacore::DataManager* data_manager =
        table.findDataManager(column_name, true);
    std::cout << "Filename: " << data_manager->fileName() << '\n';

    std::cout << '\n';

    if (!info_per_file.contains(data_manager->fileName())) {
      size_t& size = info_per_file[data_manager->fileName()].volume;
      size = FileSize(data_manager->fileName());
      if (std::filesystem::exists(data_manager->fileName() + "_TSM0"))
        size += FileSize(data_manager->fileName() + "_TSM0");
      if (std::filesystem::exists(data_manager->fileName() + "_TSM1"))
        size += FileSize(data_manager->fileName() + "_TSM1");
    }
    info_per_file[data_manager->fileName()].columns.emplace_back(column_name);
  }
  std::cout << "Total raw size: " << SizeToString(total_size) << '\n';
  std::cout << "Excluding compressed columns: "
            << SizeToString(uncompressed_size) << '\n';

  for (const auto& file_info : info_per_file) {
    const std::string size_str = SizeToString(file_info.second.volume);
    std::string padding(11 - size_str.size(), ' ');
    std::cout << padding << SizeToString(file_info.second.volume) << ' ';
    for (const std::string& col_name : file_info.second.columns)
      std::cout << " " << col_name;
    std::cout << '\n';
  }
}
