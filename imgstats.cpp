#include <aocommon/fits/fitsreader.h>
#include <aocommon/uvector.h>

#include <iostream>
#include <cmath>

using namespace aocommon;

void reportValues(const aocommon::UVector<double>& samples, bool useLimit,
                  double limitFlux, bool columnFormat) {
  double rms = 0.0;
  double avg = 0.0;

  for (aocommon::UVector<double>::const_iterator i = samples.begin();
       i != samples.end(); ++i) {
    rms += *i * *i;
    avg += *i;
  }
  avg /= samples.size();
  rms = sqrt(rms / samples.size());

  double stddev = 0.0;
  for (aocommon::UVector<double>::const_iterator i = samples.begin();
       i != samples.end(); ++i) {
    double val = *i - avg;
    stddev += val * val;
  }
  stddev = sqrt(stddev / samples.size());

  if (columnFormat) {
    std::cout << rms;
  } else {
    if (useLimit) {
      if (rms > limitFlux) std::cout << "1\n";
    } else
      std::cout << "N=" << samples.size() << "\tAvg=" << avg << "\tRMS=" << rms
                << "\tStddev=" << stddev << "\n";
  }
}

void reportValues(double frequency, const aocommon::UVector<double>& samples,
                  bool useLimit, double limitFlux, bool freqFormat) {
  if (freqFormat) std::cout << frequency << '\t';
  reportValues(samples, useLimit, limitFlux, freqFormat);
  if (freqFormat) std::cout << '\n';
}

void showSum(const aocommon::UVector<double>& image, double frequency) {
  double sum = 0.0;
  for (double v : image) {
    if (std::isfinite(v)) sum += v;
  }
  std::cout << frequency << '\t' << sum << '\n';
}

void run(aocommon::UVector<double>& image, double frequency,
         const std::string& diffFile, size_t width, size_t height,
         double truncValue, bool hasLimit, double limitRMS, bool freqOutput,
         bool hasBox, size_t boxLeft, size_t boxTop, size_t boxRight,
         size_t boxBottom) {
  if (!diffFile.empty()) {
    FitsReader diffFileReader(diffFile);
    if (width != diffFileReader.ImageWidth() ||
        height != diffFileReader.ImageHeight())
      throw std::runtime_error("Diff file is not of the same size");
    aocommon::UVector<double> diffImage(image.size());
    diffFileReader.Read(diffImage.data());
    for (size_t i = 0; i != diffImage.size(); ++i)
      image[i] = (image[i] - diffImage[i]) / M_SQRT2;
    frequency = 0.5 * (frequency + diffFileReader.Frequency());
  }

  if (hasLimit) {
    size_t boxSize = 160;
    aocommon::UVector<double> samplesAll;
    for (size_t boxY = 0; boxY != boxSize; ++boxY) {
      double* ptr = &image[(boxY + (height - boxSize) / 2) * width +
                           (width + boxSize) / 2];
      for (size_t boxX = 0; boxX != boxSize; ++boxX) {
        samplesAll.push_back(*ptr);
        ++ptr;
      }
    }
    reportValues(frequency, samplesAll, true, limitRMS, freqOutput);
  } else if (hasBox) {
    aocommon::UVector<double> samplesAll;
    for (size_t boxY = boxTop; boxY != boxBottom; ++boxY) {
      double* ptr = &image[boxY * width];
      for (size_t boxX = boxLeft; boxX != boxRight; ++boxX) {
        samplesAll.push_back(ptr[boxX]);
      }
    }
    reportValues(frequency, samplesAll, false, 0.0, freqOutput);
  } else {
    size_t boxSize = 20;
    if (freqOutput) std::cout << frequency;
    while (boxSize < width && boxSize < height) {
      aocommon::UVector<double> samplesTruncated, samplesAll;
      for (size_t boxY = 0; boxY != boxSize; ++boxY) {
        double* ptr = &image[(boxY + (height - boxSize) / 2) * width +
                             (width + boxSize) / 2];
        for (size_t boxX = 0; boxX != boxSize; ++boxX) {
          samplesAll.push_back(*ptr);
          if (*ptr <= truncValue) samplesTruncated.push_back(*ptr);
          ++ptr;
        }
      }
      if (!freqOutput) {
        std::cout << "Box size " << boxSize << '\n';
        std::cout << "All:       ";
      }
      if (freqOutput) std::cout << '\t';
      reportValues(samplesAll, false, 0.0, freqOutput);
      if (freqOutput)
        std::cout << '\t';
      else
        std::cout << "Truncated: ";
      reportValues(samplesTruncated, false, 0.0, freqOutput);
      boxSize *= 2;
    }
    if (freqOutput) std::cout << '\n';
  }
}

int main(int argc, char* argv[]) {
  if (argc < 3) {
    std::cout << "Syntax: imgstats [-sum] [-diff <fitsfile>] [-freq-output] "
                 "[-box <left> <top> <right> <bottom>] [-test <rmslimit>] "
                 "<fitsfile> <trunc value>\n\n"
                 "Output format for -freq-output:\n"
                 "[rms boxsize 20] [rms boxsize 40] [rms boxsize 80] ...\n";
  } else {
    size_t argi = 1;
    bool hasLimit = false, hasBox = false, freqOutput = false, sum = false;
    double limitRMS = 0.0;
    size_t boxLeft = 0, boxTop = 0, boxRight = 0, boxBottom = 0;
    std::string diffFile;

    while (argv[argi][0] == '-') {
      std::string p = &argv[argi][1];
      if (p == "test") {
        hasLimit = true;
        ++argi;
        limitRMS = atof(argv[argi]);
      } else if (p == "sum")
        sum = true;
      else if (p == "diff") {
        ++argi;
        diffFile = argv[argi];
      } else if (p == "freq-output") {
        freqOutput = true;
      } else if (p == "box") {
        hasBox = true;
        ++argi;
        boxLeft = atoi(argv[argi]);
        ++argi;
        boxTop = atoi(argv[argi]);
        ++argi;
        boxRight = atoi(argv[argi]);
        ++argi;
        boxBottom = atoi(argv[argi]);
      } else
        throw std::runtime_error("Unknown parameter");
      ++argi;
    }

    FitsReader reader(argv[argi]);
    double truncValue;
    if (argi + 1 < size_t(argc))
      truncValue = atof(argv[argi + 1]);
    else
      truncValue = 0.0;

    const size_t width = reader.ImageWidth(), height = reader.ImageHeight();
    aocommon::UVector<double> image(width * height);
    reader.Read(image.data());
    double frequency = reader.Frequency();

    if (sum)
      showSum(image, frequency);
    else
      run(image, frequency, diffFile, width, height, truncValue, hasLimit,
          limitRMS, freqOutput, hasBox, boxLeft, boxTop, boxRight, boxBottom);
  }
}
