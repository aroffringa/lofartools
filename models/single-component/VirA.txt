skymodel fileformat 1.1
source {
  name "VirA"
  cluster "VirA"
  component {
    type point
    position 12h30m49.4233s +12d23m28.043s
    sed {
      frequency 80 MHz
      fluxdensity Jy 1.519000e+3 0 0 0
      spectral-index { -0.7 }
    }
  }
}

