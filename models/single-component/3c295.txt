skymodel fileformat 1.1
source {
  name "3C295"
  cluster "3C295"
  component {
    type point
    position 14h11m20.519s +52d12m09.97s
    sed {
      frequency 150 MHz
      fluxdensity Jy 91 0 0 0
      spectral-index { -0.4 }
    }
  }
}
