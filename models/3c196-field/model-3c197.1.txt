skymodel fileformat 1.1
source {
  name "3c197.1"
  cluster "3c197.1"
  component {
    type gaussian
    shape 7.21221449369411 5.63545504972859 84.8
    position 08h21m33.582s 47d02m30.457s
    sed {
      frequency 125 MHz
      fluxdensity Jy 5.23056129185145062459 0 0 0
      spectral-index { -0.700918 }
    }
  }
  component {
    type gaussian
    shape 7.72105557232557 5.26406139775681 84.9
    position 08h21m33.778s 47d02m41.33s
    sed {
      frequency 125 MHz
      fluxdensity Jy 5.94443870814854937539 0 0 0
      spectral-index { -0.700918 }
    }
  }
}
