#include <iostream>
#include <stdexcept>

#include <aocommon/uvector.h>
#include <aocommon/fits/fitsreader.h>
#include <aocommon/fits/fitswriter.h>

using namespace aocommon;

int main(int argc, char* argv[]) {
  if (argc < 4)
    std::cout << "Syntax: applymask <outputfile> <inputfile> <inputmask>\n";
  else {
    std::string outputFile(argv[1]);
    std::string inputFile(argv[2]);
    std::string fitsMask(argv[3]);

    FitsReader inputReader(inputFile);
    size_t width = inputReader.ImageWidth(), height = inputReader.ImageHeight();

    std::cout << "Reading mask '" << fitsMask << "'...\n";
    FitsReader maskReader(fitsMask);
    if (maskReader.ImageWidth() > width || maskReader.ImageHeight() > height)
      throw std::runtime_error(
          "Specified fits file mask did not have same dimensions as input "
          "image!");

    aocommon::UVector<float> maskData(maskReader.ImageWidth() *
                                      maskReader.ImageHeight());
    maskReader.Read(maskData.data());

    if (maskReader.ImageWidth() != width ||
        maskReader.ImageHeight() != height) {
      aocommon::UVector<float> resizedMask(width * height, 0.0);
      size_t yOffset = (height - maskReader.ImageHeight()) / 2;
      size_t xOffset = (width - maskReader.ImageWidth()) / 2;
      for (size_t y = 0; y != maskReader.ImageHeight(); ++y) {
        for (size_t x = 0; x != maskReader.ImageWidth(); ++x) {
          resizedMask[(y + yOffset) * width + (x + xOffset)] =
              maskData[y * maskReader.ImageWidth() + x];
        }
      }
      maskData = resizedMask;
    }

    aocommon::UVector<double> imageData(width * height);
    inputReader.Read(imageData.data());

    for (size_t i = 0; i != width * height; ++i)
      imageData[i] = (maskData[i] != 0.0) ? imageData[i] : 0.0;

    FitsWriter writer(inputReader);
    writer.Write(outputFile, imageData.data());
  }
}
