#include "model/model.h"

#include <fstream>
#include <iostream>

ModelSource parseLine(const std::string& line) {
  double ra = RaDecCoord::ParseRA(line.substr(0, 12));
  double dec = RaDecCoord::ParseDec(line.substr(13, 12));
  double fInt = atof(line.substr(33, 8).c_str());
  // std::cout << "\"" << line.substr(0, 12) << "\", \"" << line.substr(13, 12)
  // << "\", \"" << line.substr(33, 8) << "\"\n";
  ModelSource s;
  ModelComponent c;
  c.SetPosRA(ra);
  c.SetPosDec(dec);
  c.SetSED(MeasuredSED(fInt, 1400000.0));
  s.AddComponent(c);
  return s;
}

int main(int argc, char* argv[]) {
  if (argc != 3) {
    std::cout << "Syntax: first2model <first-catalogue> <modelfile>\n";
  } else {
    Model model;
    std::ifstream firstFile(argv[1]);
    while (firstFile.good()) {
      std::string line;
      std::getline(firstFile, line);
      if (firstFile.good()) {
        if (!line.empty() && line[0] != '#') model.AddSource(parseLine(line));
      }
    }
    model.Save(argv[2]);
  }
}
